package com.moderm.smartsport.utils

import io.ktor.client.call.TypeInfo
import io.ktor.client.features.json.JsonSerializer
import io.ktor.http.ContentType
import io.ktor.http.content.OutgoingContent
import io.ktor.http.content.TextContent
import io.ktor.utils.io.core.Input
import io.ktor.utils.io.core.readText
import kotlinx.serialization.KSerializer
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import kotlin.reflect.KClass

class JsonKotlinxSerializer : JsonSerializer {
    val mappers = mutableMapOf<KClass<Any>, KSerializer<Any>>()

    /**
     * Set mapping from [type] to generated [KSerializer].
     */
    inline fun <reified T> setMapper(serializer: KSerializer<T>) {
        mappers[T::class as KClass<Any>] = serializer as KSerializer<Any>
    }

    @UseExperimental(UnstableDefault::class)
    override fun write(data: Any, contentType: ContentType): OutgoingContent {
        val content = Json.nonstrict.stringify(mappers[data::class]!!, data)
        return TextContent(content, ContentType.Application.Json)
    }

    @UseExperimental(UnstableDefault::class)
    override fun read(type: TypeInfo, body: Input): Any {
        val mapper =
            mappers[type.type] ?: throw Throwable("Can't find mapper for ${type.type.simpleName}")
        return Json.nonstrict.parse(mapper, body.readText())
    }
}