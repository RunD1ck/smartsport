package com.modern.smartsport.utils

expect fun logException(tag: String, text: String, exception: Throwable?)
expect fun log(tag: String, text: String)