package com.modern.smartsport.data

interface ChatterApi {
    /**
     * Создать пост
     *
     * @param text - текст для поста
     * @param byteArray - изображение в виде массива байт
     * @param fileName - название файла
     * @return описание поста в JSON строке
     */
    suspend fun createPost(text: String, byteArray: ByteArray, fileName: String): String
}