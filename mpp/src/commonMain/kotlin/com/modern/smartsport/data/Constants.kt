package com.moderm.smartsport.data

interface Constants {
    object API {
        private val BASE_URL by lazy { "https://api.chatter.host" }

        //send photo
        val SEND_PHOTO_URL by lazy { "$BASE_URL/api/v1/media/create" }
    }
}