package com.modern.smartsport.data

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class Error(
    val message: String
)