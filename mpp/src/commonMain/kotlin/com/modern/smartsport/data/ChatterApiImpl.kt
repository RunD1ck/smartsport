package com.modern.smartsport.data

import com.moderm.smartsport.data.Constants.API.SEND_PHOTO_URL
import com.moderm.smartsport.extension.appendFile
import com.moderm.smartsport.utils.JsonKotlinxSerializer
import com.modern.smartsport.storage.Prefs
import io.ktor.client.HttpClient
import io.ktor.client.features.DefaultRequest
import io.ktor.client.features.HttpCallValidator
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.logging.DEFAULT
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import io.ktor.client.request.forms.MultiPartFormDataContent
import io.ktor.client.request.forms.formData
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.http.isSuccess
import io.ktor.utils.io.readUTF8Line
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.parse

fun provideChatterApi(): ChatterApi = ChatterApiImpl()

class ChatterApiImpl : ChatterApi {


    private val prefs: Prefs by lazy {
        Prefs()
    }
    @UseExperimental(ImplicitReflectionSerializer::class)
    private val client by lazy {
        HttpClient {
            install(JsonFeature) {
                serializer = JsonKotlinxSerializer().apply {
                }
            }
            install(HttpCallValidator) {
                validateResponse {
                    if (!it.status.isSuccess()) {
                        val response = it.content.readUTF8Line().toString()
                        val error = Json.parse<Error>(response)
                        throw Exception(error.message)
                    }
                }
            }
            install(DefaultRequest) {
                header(
                    "Authorization",
                    "Bearer ${prefs.getString("token")}"
                )
            }
            install(Logging) {
                logger = Logger.DEFAULT
                level = LogLevel.ALL
            }
        }
    }

    override suspend fun createPost(text: String, byteArray: ByteArray, fileName: String): String {
        return client.post {
            url(SEND_PHOTO_URL)
            body = MultiPartFormDataContent(formData {
                append("text", "text")
                appendFile("photo", byteArray, fileName)
            })
        }
    }
}