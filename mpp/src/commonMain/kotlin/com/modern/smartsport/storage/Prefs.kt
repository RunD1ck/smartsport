package com.modern.smartsport.storage


expect class Prefs() {
    fun putString(key: String, value: String)
    fun getString(key: String): String?
}