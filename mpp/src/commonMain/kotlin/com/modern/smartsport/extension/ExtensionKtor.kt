package com.moderm.smartsport.extension

import io.ktor.client.request.forms.FormBuilder
import io.ktor.http.Headers
import io.ktor.http.HttpHeaders
import io.ktor.utils.io.core.buildPacket
import io.ktor.utils.io.core.writeFully

/**
 * Добавляет файл в виде массива байт в [FormBuilder] для formData
 *
 * @param key - значение параметра formData
 * @param byteArray - представление файла
 * @param fileName - название файла
 */
fun FormBuilder.appendFile(key: String, byteArray: ByteArray, fileName: String) {
    appendInput(
        key,
        headers = Headers.build {
            append(HttpHeaders.ContentDisposition, "filename=${fileName}")
        },
        size = byteArray.size.toLong()
    ) {
        buildPacket {
            writeFully(byteArray)
        }
    }
}