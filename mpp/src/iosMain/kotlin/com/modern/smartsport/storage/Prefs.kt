package com.modern.smartsport.storage

import platform.Foundation.NSUserDefaults

actual class Prefs {
    private val delegate: NSUserDefaults = NSUserDefaults.standardUserDefaults()

    actual fun putString(key: String, value: String) {
        delegate.setObject(value, key)
    }

    actual fun getString(key: String): String? =
        delegate.stringForKey(key) ?: "Nothing"
}