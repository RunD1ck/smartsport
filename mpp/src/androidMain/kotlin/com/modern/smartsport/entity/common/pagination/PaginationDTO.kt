package com.modern.smartsport.entity.common.pagination

data class PaginationDTO(var offset: String, var size: String)