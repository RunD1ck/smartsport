package com.modern.smartsport.storage

import android.content.Context
import com.modern.smartsport.application


actual class Prefs {

    private fun getSharedPreferences(prefsName:String = "MAIN_PREF") =
        application.getSharedPreferences(prefsName,Context.MODE_PRIVATE)

    actual fun putString(key: String, value: String) {
        getSharedPreferences().edit().putString(key,value).apply()
    }

    actual fun getString(key: String): String? {
        return getSharedPreferences().getString(key,"Nothing")
    }
}