package com.modern.smartsport.utils

import android.util.Log
import javax.security.auth.login.LoginException

actual fun logException(tag: String, text: String, exception: Throwable?) {
    Log.e(tag, text)
}

actual fun log(tag: String, text: String) {
    Log.v(tag, text)
}