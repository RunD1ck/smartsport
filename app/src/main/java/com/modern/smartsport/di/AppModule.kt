package com.modern.smartsport.di

import com.modern.smartsport.model.interactor.auth.CheckSmsCodeInteractor
import com.modern.smartsport.model.interactor.auth.PhoneLoginInteractor
import com.modern.smartsport.model.interactor.home.plans.traning.TraningInteractor
import com.modern.smartsport.model.interactor.home.calendar.CalendarInteractor
import com.modern.smartsport.model.interactor.home.calendar.newevent.selectplace.SelectPlaceInteractor
import com.modern.smartsport.model.interactor.home.objects.ObjectsInteractor
import com.modern.smartsport.model.interactor.home.profile.my.achievement.AchievementInteractor
import com.modern.smartsport.ui.navigation.NavControllerNavigator
import org.koin.core.qualifier.named
import org.koin.dsl.module

val appModule = module {

    //Navigation
    scope(named(navScopeScopeName)) {
        scoped { NavControllerNavigator() }
    }

    //My Plan
    factory { TraningInteractor() }
    //Calendar
    factory { CalendarInteractor() }
    //Achievement
    factory { AchievementInteractor() }
    //SelectPlace
    factory { SelectPlaceInteractor() }
    //PhoneLogin
    factory { PhoneLoginInteractor() }
    //СheckSmsCode
    factory { CheckSmsCodeInteractor() }
    //Objects
    factory { ObjectsInteractor() }
}

//Navigation
const val navScopeScopeName = "NAV_SCOPE"
const val globalNavScopeId = "NAVIGATION_GLOBAL"

/**
 * Формирует имя скоупа на labl'у графа
 *
 * @param graphLabel имя скоупа с приставкой NAVIGATION_
 */
fun getNavScopeName(graphLabel: CharSequence) = "NAVIGATION_$graphLabel"

