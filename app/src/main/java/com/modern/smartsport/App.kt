package com.modern.smartsport

import android.app.Application
import android.util.Log
import com.modern.smartsport.di.appModule
import org.koin.core.context.startKoin
import java.lang.Exception

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(appModule)
        }

    }


}