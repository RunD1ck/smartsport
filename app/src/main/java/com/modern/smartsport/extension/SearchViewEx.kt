package com.modern.smartsport.extension

import android.widget.SearchView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * Слушает изменение текста в @[SearchView] с заданным @[debounce]
 *
 * @param debounce интервал через который будет отдаваться текст
 * @param query текст запроса
 */
fun SearchView.onDebounceQueryChange(debounce: Long, query: (String) -> Unit) {

    var searchFor = ""

    setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextChange(q: String): Boolean {
            val searchText = q.trim()

            if (searchText != searchFor) {
                searchFor = searchText

                CoroutineScope(Dispatchers.Main).launch {
                    delay(debounce)  //debounce timeOut
                    if (searchText != searchFor)
                        return@launch

                    query(searchText)
                }
            }
            return false
        }

        override fun onQueryTextSubmit(q: String): Boolean {
            return false
        }
    })
}