package com.modern.smartsport.extension

import android.util.Log
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Month
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.util.*

/**
 * Функция для получения текущего дня недели в строке
 *
 * @return
 */
fun Calendar.getChooseWeekDay(day: Int = 0): String {
    this.add(Calendar.DAY_OF_WEEK, day)
    val date = this.time
    return SimpleDateFormat("EE", Locale("ru", "RU")).format(date.time).toUpperCase()
}


/**
 * Возвращает текущее время в милисекундах
 *
 * @return
 */
fun Calendar.getCurrentTime(): Long {
    return this.time.time
}

/**
 * Возвращает текущий день в месяце - количество указанных дней (временная функция)
 *
 * @param day - параметр для получение currentMonthDay - day номера
 * @return
 */
fun Calendar.getChooseMonthDay(day: Int = 0): Int {
    this.add(Calendar.DAY_OF_MONTH, day)
    return this.get(Calendar.DAY_OF_MONTH)
}


/**
 * Функция для проверки вхождения текущего времени в указанный диапазон
 *
 * @param dateFrom - дата начала диапазона времени
 * @param dateTo - дата конца диапазона времени
 * @return
 */
fun Calendar.isIncludeInRange(dateFrom: Date?, dateTo: Date?) =
    if (dateFrom != null && dateTo != null) this.time >= dateFrom && this.time < dateTo else false


/**
 * Функция для перевода времени в текущую дату с этим временем
 *
 * @param time время
 * @return
 */
fun Calendar.convertCurrentTimeToDate(time: String): Date? {
    val currentDate =
        SimpleDateFormat("yyyy:MM:dd", Locale("ru", "RU")).format(this.time)
    val date = "$currentDate $time"
    val format = SimpleDateFormat("yyyy:MM:dd HH:mm")
    return format.parse(date)
}