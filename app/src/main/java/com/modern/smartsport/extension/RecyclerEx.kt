package com.modern.smartsport.extension

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * устанавливает вертикальный LinearLayoutManager без реверса
 *
 */
fun RecyclerView.setLinearDefaultLayoutManager(orientation: Int = RecyclerView.VERTICAL) {
    this.layoutManager = LinearLayoutManager(this.context, orientation, false)
}