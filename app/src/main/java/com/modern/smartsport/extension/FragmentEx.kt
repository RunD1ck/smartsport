package com.modern.smartsport.extension

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.modern.smartsport.R
import com.modern.smartsport.ui.MainActivity
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Добавляет заголовок тулбару
 *
 * @param title - заголовок тулбара
 */
fun Fragment.setTitle(title: CharSequence) {
    (activity as MainActivity).supportActionBar?.title = title
}

/**
 * Изменяет видимость тулбара
 *
 * @param visible видимость турбара
 */
fun Fragment.visibleToolbar(visible: Boolean) {
    (activity as AppCompatActivity).toolbar?.isVisible = visible
}

/**
 * Делает тулбар прозрачным  или оставляет белым
 *
 * @param enable - флаг для изменения фона тулбара
 */
fun Fragment.translucentToolbar(enable: Boolean = false) {
    (activity as MainActivity).toolbar?.setBackgroundColor(
        if (enable) Color.TRANSPARENT
        else Color.WHITE
    )
}


/**
 * Изменяет задний фон тулбара
 *
 * @param resId - id фона
 */
fun Fragment.setBackgroundToolbar(resId: Int) {
    (activity as MainActivity).toolbar?.setBackgroundResource(resId)
}

/**
 * Делает стрелку "назад" белой
 *
 * @param isSet - флаг для изменения стрелки "назад"
 */
fun Fragment.setWhiteBackArrow(isSet:Boolean){
    if (isSet)(activity as MainActivity).toolbar?.setNavigationIcon(R.drawable.ic_white_back_arrow)
}

/**
 * получаем значение dimen в Int из ресурсов
 *
 * @param res - ресурс
 * @return
 */
fun Fragment.getDimen(res: Int): Int = resources.getDimension(res).toInt()