package com.modern.smartsport.extension

import android.app.Activity
import android.graphics.Color
import androidx.core.view.isVisible
import com.modern.smartsport.ui.MainActivity
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Изменяет видимость тулбара
 *
 * @param visible видимость турбара
 */
fun Activity.visibleToolbar(visible: Boolean) {
    (this as MainActivity).toolbar?.isVisible = visible
}

/**
 * Изменяет цвет тулбара
 *
 * @param enable белый или прозрачный
 */
fun Activity.translucentToolbar(enable: Boolean = false) {
    (this as MainActivity).toolbar?.setBackgroundColor(
        if (enable) Color.TRANSPARENT
        else Color.WHITE
    )
}

