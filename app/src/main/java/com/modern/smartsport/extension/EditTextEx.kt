package com.modern.smartsport.extension

import android.widget.EditText
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import java.util.*

/**
 * добавление маски к EditText с помощью библиотеки RedMadRobot
 *
 * @param mask - паттерн маски
 * @param changeText - лямбда которая вызывается при изменение текста
 */
fun EditText.setMask(
    mask: String,
    changeText: (String) -> Unit
) {

    val affineFormats: MutableList<String> = ArrayList()
    affineFormats.add(mask)

    val listener =
        MaskedTextChangedListener.installOn(
            this,
            mask,
            affineFormats, AffinityCalculationStrategy.PREFIX,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(
                    maskFilled: Boolean,
                    extractedValue: String,
                    formattedText: String
                ) {
                    changeText(extractedValue)
                }
            }
        )

    this.hint = listener.placeholder()
}