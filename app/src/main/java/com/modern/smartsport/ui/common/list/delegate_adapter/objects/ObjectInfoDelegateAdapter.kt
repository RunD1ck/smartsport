package com.modern.smartsport.ui.common.list.delegate_adapter.objects

import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.objects.ObjectInfoItem
import com.modern.smartsport.entity.home.objects.ObjectTimeInfo
import com.modern.smartsport.extension.setLinearDefaultLayoutManager

fun objectInfoAdapterDelegate() =
    adapterDelegate<ObjectInfoItem, Any>(R.layout.rv_object_info_item) {

        val tvSportKind: TextView = findViewById(R.id.tvSportKind)
        val tvNameObject: TextView = findViewById(R.id.tvNameObject)
        val tvStreetObject: TextView = findViewById(R.id.tvStreetObject)
        val tvRating: TextView = findViewById(R.id.tvRating)
        val tvVotesCount: TextView = findViewById(R.id.tvVotesCount)
        val rvTimeInfo: RecyclerView = findViewById(R.id.rvTimeInfo)

        bind {
            tvSportKind.text = item.sportKind.toUpperCase()
            tvNameObject.text = item.nameObject
            tvStreetObject.text = item.streetObject
            tvRating.text = item.rating.toString()
            tvVotesCount.text =
                context.getString(R.string.fragment_object_votes, item.votesCount.toString())
            rvTimeInfo.setLinearDefaultLayoutManager(orientation = RecyclerView.HORIZONTAL)
            val adapter = ListDelegationAdapter<List<Any>>(timeInfoAdapterDelegate())
            adapter.items = listOf(
                ObjectTimeInfo(R.drawable.ic_pedestrian, item.timeOnFoot),
                ObjectTimeInfo(R.drawable.ic_car, item.timeOnCar),
                ObjectTimeInfo(R.drawable.ic_bus, item.timeOnPublicTransport)
            )
            rvTimeInfo.adapter = adapter
            rvTimeInfo.isLayoutFrozen = true

        }
    }

private fun timeInfoAdapterDelegate() =
    adapterDelegate<ObjectTimeInfo, Any>(R.layout.rv_time_object_info_item) {

        val ivIcon: ImageView = findViewById(R.id.ivIcon)
        val tvTime: TextView = findViewById(R.id.tvTime)

        bind {
            ivIcon.setImageResource(item.iconId)
            tvTime.text = context.getString(R.string.fragment_object_time_info, item.time)
        }
    }