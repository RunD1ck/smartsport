package com.modern.smartsport.ui.common.list.decorators

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.modern.smartsport.entity.home.calendar.CalendarLineCurrentItem

fun RecyclerView.addMarginDecoratorWithList(
    leftResId: Int = 0,
    topResId: Int = 0,
    rightResId: Int = 0,
    bottomResId: Int = 0,
    list: List<Any>

) {
    this.addItemDecoration(
        MarginWithListItemDecorator(
            leftResId,
            topResId,
            rightResId,
            bottomResId,
            list
        )
    )
}

class MarginWithListItemDecorator(
    private var leftResId: Int = 0,
    private var topResId: Int = 0,
    private var rightResId: Int = 0,
    private var bottomResId: Int = 0,
    var list: List<Any>
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        val position = parent.getChildAdapterPosition(view)
        fun getResFromDimen(resId: Int) =
            if (resId == 0) 0 else parent.resources.getDimension(resId).toInt()

        val condition = position < list.size - 1 && list[position + 1] is CalendarLineCurrentItem
        val isCalendarLineCurrentItem = list[position] is CalendarLineCurrentItem

        outRect.top = getResFromDimen(topResId)
        outRect.left =
            if (isCalendarLineCurrentItem) 0 else getResFromDimen(leftResId)
        outRect.right = getResFromDimen(rightResId)
        outRect.bottom =
            if (condition || isCalendarLineCurrentItem) 0 else getResFromDimen(bottomResId)
    }
}