package com.modern.smartsport.ui.common.list.decorators

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Добавляет одинаковые отступы для каждого элемента, только для 1 элемента левый отступ будет добавлен
 *
 * @param leftResId - левый отступ
 * @param topResId - верхний отступ
 * @param rightResId - правый отступ
 * @param bottomResId - нижний отступ
 */
fun RecyclerView.addMarginHorizontalItemDecorator(
    leftResId: Int = 0,
    topResId: Int = 0,
    rightResId: Int = 0,
    bottomResId: Int = 0
) {
    this.addItemDecoration(
        MarginHorizontalDecorator(
            leftResId,
            topResId,
            rightResId,
            bottomResId
        )
    )
}

/**
 * Добавляет одинаковые отступы для каждого элемента, только для 1 элемента левый отступ будет добавлен
 *
 * @param marginResId - левый отступ
 */
fun RecyclerView.addMarginHorizontalItemDecorator(
    marginResId: Int = 0
) {
    this.addItemDecoration(
        MarginVerticalItemDecorator(
            marginResId,
            marginResId,
            marginResId,
            marginResId
        )
    )
}

class MarginHorizontalDecorator(
    private var leftResId: Int = 0,
    private var topResId: Int = 0,
    private var rightResId: Int = 0,
    private var bottomResId: Int = 0
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        fun getResFromDimen(resId: Int) =
            if (resId == 0) 0 else parent.resources.getDimension(resId).toInt()

        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.left = getResFromDimen(leftResId)
        }

        outRect.top = getResFromDimen(topResId)
        outRect.right = getResFromDimen(rightResId)
        outRect.bottom = getResFromDimen(bottomResId)
    }
}