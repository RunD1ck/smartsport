package com.modern.smartsport.ui.common.adapter.calendar

import android.content.Context
import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager
import com.hannesdorfmann.adapterdelegates4.paging.PagedListDelegationAdapter
import com.modern.smartsport.entity.home.calendar.BaseCalendar
import com.modern.smartsport.entity.home.calendar.CalendarEvent
import com.modern.smartsport.ui.common.list.delegate_adapter.calendar.calendarEventAdapterDelegate
import com.modern.smartsport.ui.common.list.delegate_adapter.calendar.calendarLineCurrentAdapterDelegate

fun BaseCalendar.isSame(baseCalendar: BaseCalendar) =
    if (baseCalendar is CalendarEvent && this is CalendarEvent) this.id == baseCalendar.id else false

class CalendarRVAdapter(private val context: Context) : PagedListDelegationAdapter<BaseCalendar>(
    AdapterDelegatesManager<List<BaseCalendar>>().apply {
        addDelegate(calendarEventAdapterDelegate())
        addDelegate(calendarLineCurrentAdapterDelegate())
    },
    object : DiffUtil.ItemCallback<BaseCalendar>() {
        override fun areItemsTheSame(oldItem: BaseCalendar, newItem: BaseCalendar): Boolean =
            oldItem.isSame(newItem)

        override fun areContentsTheSame(oldItem: BaseCalendar, newItem: BaseCalendar): Boolean =
            oldItem.isSame(newItem)

    }
)