package com.modern.smartsport.ui.common.list.delegate_adapter.objects

import android.widget.CheckBox
import android.widget.TextView
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.objects.ServiceFilter

fun objectServicesAdapterDelegate() =
    adapterDelegate<ServiceFilter, Any>(R.layout.rv_services_filter_item) {

        val tvTitle: TextView = findViewById(R.id.tvTitle)
        val cbSelect: CheckBox = findViewById(R.id.cbSelect)



        bind {
            tvTitle.text = item.title
            cbSelect.isChecked = item.isSelected

            cbSelect.setOnCheckedChangeListener { _, isChecked ->
                item.isSelected = isChecked
            }
        }

    }