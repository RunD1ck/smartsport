package com.modern.smartsport.ui.fragment.home.calendar

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.calendar.BaseCalendar
import com.modern.smartsport.entity.home.calendar.CalendarDay
import com.modern.smartsport.presentation.viewmodel.home.calendar.CalendarViewModel
import com.modern.smartsport.ui.common.BaseFragment
import com.modern.smartsport.ui.common.adapter.calendar.CalendarRVAdapter
import com.modern.smartsport.ui.common.list.decorators.MarginWithListItemDecorator
import com.modern.smartsport.ui.common.list.decorators.addMarginDecoratorWithList
import kotlinx.android.synthetic.main.fragment_calendar.*

class CalendarFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_calendar

    private val calendarRVAdapter: CalendarRVAdapter by lazy {
        CalendarRVAdapter(requireContext())
    }

    private val viewModel: CalendarViewModel by lazy {
        ViewModelProviders.of(this).get(CalendarViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvCalendar.layoutManager = LinearLayoutManager(requireContext())
        rvCalendar.adapter = calendarRVAdapter

        btnNewEvent.setOnClickListener {
            viewModel.onBtnEventClick()
        }

        val decorator = MarginWithListItemDecorator(bottomResId = R.dimen.margin_4,list = listOf<BaseCalendar>())
        rvCalendar.addItemDecoration(decorator)

        viewModel.calendarEventsList.observe(viewLifecycleOwner, Observer<PagedList<BaseCalendar>> {
            calendarRVAdapter.submitList(it)
            decorator.list = it
        })

    }
}
