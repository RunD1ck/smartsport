package com.modern.smartsport.ui.fragment.home.profile.my

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.profile.my.ProfileViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_profile
    override var menuResId: Int? = R.menu.profile_menu

    private val viewModel: ProfileViewModel by lazy {
        ViewModelProviders.of(this).get(ProfileViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnAchievement.setOnClickListener {
            viewModel.onAchievementBtnClick()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.exitItem -> viewModel.onExitItemClick()
            R.id.myFormItem -> findNavController().navigate(R.id.action_profileFragment_to_myFormFragment)
        }
        return true
    }
}
