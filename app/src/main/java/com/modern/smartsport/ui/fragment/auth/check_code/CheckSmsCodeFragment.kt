package com.modern.smartsport.ui.fragment.auth.check_code

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.auth.CheckSmsCodeViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_check_sms_code.*

class CheckSmsCodeFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_check_sms_code

    private val viewModel: CheckSmsCodeViewModel by lazy {
        ViewModelProviders.of(this).get(CheckSmsCodeViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etCode.setOnPinEnteredListener {
            viewModel.onEtCodeEntered(it.toString())
        }

        tvClose.setOnClickListener {
            viewModel.onTvCloseClick()
        }
    }
}
