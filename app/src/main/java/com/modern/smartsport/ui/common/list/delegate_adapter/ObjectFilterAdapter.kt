package com.modern.smartsport.ui.common.list.delegate_adapter

import android.widget.Button
import androidx.core.content.ContextCompat
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.objects.ObjectFilter


enum class ObjectFilterPayload {
    SELECTED,
    NO_SELECTED
}

fun objectFilterDelegateAdapter(itemClickListener: (Int) -> Unit) =
    adapterDelegate<ObjectFilter, Any>(R.layout.item_object_filter) {

        val btnFilter: Button = findViewById(R.id.btnFilter)
        btnFilter.setOnClickListener {
            itemClickListener(adapterPosition)
        }


        bind { payloads ->
            if (payloads.isNotEmpty()) {
                when (payloads[0]) {
                    ObjectFilterPayload.SELECTED -> {
                        btnFilter.isSelected = true
                        btnFilter.setTextColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.white
                            )
                        )
                    }

                    ObjectFilterPayload.NO_SELECTED -> {
                        btnFilter.isSelected = false
                        btnFilter.setTextColor(
                            ContextCompat.getColor(
                                itemView.context,
                                R.color.gunmetal
                            )
                        )
                    }
                }
            } else {
                btnFilter.text = item.name
            }
        }
    }