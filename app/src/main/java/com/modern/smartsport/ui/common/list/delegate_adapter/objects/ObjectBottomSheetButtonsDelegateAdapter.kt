package com.modern.smartsport.ui.common.list.delegate_adapter.objects

import android.view.View
import android.widget.Button
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.objects.BottomSheetButtons

fun objectBottomSheetButtonsAdapterDelegate(onFind: (View) -> Unit, onClear: () -> Unit) =
    adapterDelegate<BottomSheetButtons, Any>(R.layout.rv_bottom_sheet_buttons) {

        val btnFind: Button = findViewById(R.id.btnFind)
        val btnClear:Button = findViewById(R.id.btnClear)

        bind {

            btnFind.setOnClickListener{
                onFind(it)
            }

            btnClear.setOnClickListener {
                onClear()
            }
        }

    }