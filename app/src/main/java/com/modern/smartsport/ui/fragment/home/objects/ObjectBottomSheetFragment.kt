package com.modern.smartsport.ui.fragment.home.objects

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.objects.AllCategoriesItem
import com.modern.smartsport.entity.home.objects.ServiceFilter
import com.modern.smartsport.extension.setLinearDefaultLayoutManager
import com.modern.smartsport.ui.common.list.delegate_adapter.objects.*
import kotlinx.android.synthetic.main.bottom_sheet_object_filter.*
import kotlinx.android.synthetic.main.rv_distance_filter_item.view.*

class ObjectBottomSheetFragment : BottomSheetDialogFragment() {

    private val adapter: ListDelegationAdapter<List<Any>> by lazy {
        ListDelegationAdapter<List<Any>>(
            objectServicesAdapterDelegate(),
            objectDistanceAdapterDelegate(
                onAccept = { dismiss() },
                onClear = {
                    it.seekBar.progress = 0
                }),
            objectInfoAdapterDelegate(),
            objectAllCategoriesAdapterDelegate { _, position ->
                listAllCategories.forEachIndexed { index, allCategoriesItem ->
                    if (allCategoriesItem.isSelected) {
                        allCategoriesItem.isSelected = false
                        rvFilters.adapter?.notifyItemChanged(index)
                    }
                }
                listAllCategories[position].isSelected = true
                rvFilters.adapter?.notifyItemChanged(position)
            },
            objectBottomSheetButtonsAdapterDelegate(
                onFind = { dismiss() },
                onClear = {
                    adapter.items.mapIndexed { index, item ->
                        if (item is ServiceFilter) {
                            if (item.isSelected) {
                                item.isSelected = false
                                adapter.notifyItemChanged(index)
                            }
                        }
                    }
                })
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottom_sheet_object_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvFilters.setLinearDefaultLayoutManager()

        rvFilters.adapter = adapter

    }

    fun setList(list: List<Any> = listAllCategories) {
        adapter.items = list
        adapter.notifyDataSetChanged()
    }


    private val listAllCategories: List<AllCategoriesItem> by lazy {
        listOf(
            AllCategoriesItem(
                R.drawable.ic_search_filter,
                "Все категории",
                null,
                true
            ),
            AllCategoriesItem(
                R.drawable.ic_heart_filter,
                "Групповой фитнесс",
                "Йога, бокс, пилатес и т.д.",
                false
            ),
            AllCategoriesItem(
                R.drawable.ic_flower_filter,
                "Красота и здоровье",
                "Массаж, медитация и т.д.",
                false
            ),
            AllCategoriesItem(
                R.drawable.ic_dumbbell_filter,
                "Тренировки",
                "Занятия в тренажерном зале",
                false
            )
        )
    }


}