package com.modern.smartsport.ui.common.list.delegate_adapter.calendar

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.calendar.BaseCalendar
import com.modern.smartsport.entity.home.calendar.CalendarLineCurrentItem

fun calendarLineCurrentAdapterDelegate() =
    adapterDelegate<CalendarLineCurrentItem, BaseCalendar>(R.layout.rv_calendar_line_current_event_item) {

    }