package com.modern.smartsport.ui.common.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.modern.smartsport.R

class CalendarLineView : View {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private val linePaint: Paint by lazy {
        Paint()
    }

    init {
        linePaint.color = ContextCompat.getColor(context, R.color.baltic_sea)
    }

    private val defaultBarWidth: Int =
        resources.getDimension(R.dimen.volume_bar_default_width).toInt()
    private val defaultBarHeight: Int =
        resources.getDimension(R.dimen.volume_bar_default_height).toInt()

    override fun onDraw(canvas: Canvas?) {
        linePaint.strokeWidth =
            height.toFloat() / 5 //Зависимость между размером кружка и линии  = 8/1.5 (из размеров в дизайне)
        canvas?.drawLine(
            height.toFloat() / 2,
            height.toFloat() / 2,
            width.toFloat(),
            height.toFloat() / 2,
            linePaint
        )
        canvas?.drawCircle(
            height.toFloat() / 2,
            height.toFloat() / 2,
            height.toFloat() / 2,
            linePaint
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        val width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> defaultBarWidth
            MeasureSpec.UNSPECIFIED -> defaultBarWidth
            else -> defaultBarWidth
        }

        val height = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> defaultBarHeight
            MeasureSpec.UNSPECIFIED -> defaultBarHeight
            else -> defaultBarHeight
        }

        setMeasuredDimension(width, height)
    }

}