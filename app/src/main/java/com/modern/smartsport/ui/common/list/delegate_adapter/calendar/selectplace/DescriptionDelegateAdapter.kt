package com.modern.smartsport.ui.common.list.delegate_adapter.calendar.selectplace

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.calendar.selectplace.FokDescriptionItem

fun descriptionAdapterDelegate() =
    adapterDelegate<FokDescriptionItem, FokDescriptionItem>(R.layout.rv_select_place_description_item) {

        val tvTitle: TextView = findViewById(R.id.tvTitle)
        val tvDescription: TextView = findViewById(R.id.tvDescription)
        val ivTriangleDown: ImageView = findViewById(R.id.ivTriangleDown)

        bind {
            tvTitle.text = item.title
            tvDescription.text = item.description
            if (item.title == getString(R.string.fragment_select_place_time)) {
                ivTriangleDown.visibility = View.VISIBLE
            }
        }
    }