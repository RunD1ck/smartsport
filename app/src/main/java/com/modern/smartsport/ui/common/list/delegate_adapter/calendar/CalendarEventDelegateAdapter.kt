package com.modern.smartsport.ui.common.list.delegate_adapter.calendar

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.calendar.BaseCalendar
import com.modern.smartsport.entity.home.calendar.CalendarEvent

fun calendarEventAdapterDelegate() =
    adapterDelegate<CalendarEvent, BaseCalendar>(R.layout.rv_calendar_event_item) {

        val tvTitle: TextView = findViewById(R.id.tvTitle)
        val tvTime: TextView = findViewById(R.id.tvTime)
        val tvDescription: TextView = findViewById(R.id.tvDescription)
        val view: View = findViewById(R.id.view)
        val tvMonthDay: TextView = findViewById(R.id.tvMonthDay)
        val tvWeekDay: TextView = findViewById(R.id.tvWeekDay)

        bind {
            tvTitle.text = item.title
            tvTime.text = getString(R.string.hours_time, item.timeFrom, item.timeTo)
            tvDescription.text = item.description
            if (item.monthDay != null && item.weekDay != null) {
                tvMonthDay.visibility = View.VISIBLE
                tvWeekDay.visibility = View.VISIBLE
                tvMonthDay.text = item.monthDay.toString()
                tvWeekDay.text = item.weekDay
                if (item.isCurrentTime) {
                    tvMonthDay.setBackgroundResource(R.drawable.bg_calendar_month_day)
                    tvMonthDay.setTextColor(Color.WHITE)
                }
            }
            view.background = ContextCompat.getDrawable(context, item.eventType.backgroundRes)
        }
    }
