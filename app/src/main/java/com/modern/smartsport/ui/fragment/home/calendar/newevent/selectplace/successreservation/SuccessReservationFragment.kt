package com.modern.smartsport.ui.fragment.home.calendar.newevent.selectplace.successreservation

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.selectplace.successreservation.SuccessReservationViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_success_reservation.*

class SuccessReservationFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_success_reservation

    private val viewModel: SuccessReservationViewModel by lazy {
        ViewModelProviders.of(this).get(SuccessReservationViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnMap.setOnClickListener {
            findNavController().navigate(R.id.action_successReservationFragment_to_selectPlaceFragment)
        }

        btnCalendar.setOnClickListener {
            findNavController().navigate(R.id.action_successReservationFragment_to_calendarFragment)
        }

    }
}
