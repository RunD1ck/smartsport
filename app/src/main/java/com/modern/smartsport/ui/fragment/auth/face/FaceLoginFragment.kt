package com.modern.smartsport.ui.fragment.auth.face

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.auth.face.FaceLoginViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_face_login.*

class FaceLoginFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_face_login

    private val viewModel: FaceLoginViewModel by lazy {
        ViewModelProviders.of(this).get(FaceLoginViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.state.observe(viewLifecycleOwner, Observer {
            when (it) {
                FaceLoginViewModel.State.FinishFaceLogin -> {
                    btnFinish.isVisible = true
                    clFaceLogin.setBackgroundResource(R.drawable.finish_face_login_screen)
                }
            }
        })

        btnScan.setOnClickListener {
            viewModel.setOnScanBtnClick()
        }

        btnFinish.setOnClickListener {
            viewModel.setOnFinishBtnClick()
        }
    }
}
