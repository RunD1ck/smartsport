package com.modern.smartsport.ui.fragment.home.profile.expert

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.profile.expert.ProfileExpertViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_profile_expert.*
import org.koin.core.KoinComponent

class ProfileExpertFragment : BaseFragment(), KoinComponent {

    private val args: ProfileExpertFragmentArgs by navArgs()

    override val layoutResId = R.layout.fragment_profile_expert

    private val viewModel: ProfileExpertViewModel by lazy {
        ViewModelProviders.of(this).get(ProfileExpertViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Тренер
        ivExpertScreen.setImageResource(R.drawable.profile_expert_train)


        //Диетолог
        //ivExpertScreen.setImageResource(R.drawable.profile_expert_nutrition)

        allComments.setOnClickListener {
            viewModel.allCommentsBtnClick()
        }

        btnAllScholars.setOnClickListener {
            findNavController().navigate(R.id.action_profileExpertFragment_to_allScholarsFragment)
        }
    }
}
