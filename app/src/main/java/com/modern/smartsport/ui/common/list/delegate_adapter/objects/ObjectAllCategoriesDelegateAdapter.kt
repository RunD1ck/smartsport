package com.modern.smartsport.ui.common.list.delegate_adapter.objects

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.objects.AllCategoriesItem

fun objectAllCategoriesAdapterDelegate(onViewClick: (View, Int) -> Unit) =
    adapterDelegate<AllCategoriesItem, Any>(R.layout.rv_all_categories_filter_item) {

        val tvTitle: TextView = findViewById(R.id.tvTitle)
        val ivIcon: ImageView = findViewById(R.id.ivIcon)
        val tvDescription: TextView = findViewById(R.id.tvDescription)
        val ivCheck: ImageView = findViewById(R.id.ivCheck)

        bind {
            tvTitle.text = item.title
            ivIcon.setImageResource(item.iconId)
            if (item.description != null) {
                tvDescription.visibility = View.VISIBLE
                tvDescription.text = item.description
            } else {
                tvDescription.visibility = View.GONE
            }

            ivCheck.isVisible = item.isSelected

            itemView.setOnClickListener {
                onViewClick(it, adapterPosition)
            }

        }
    }