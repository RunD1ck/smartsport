package com.modern.smartsport.ui.fragment.home.calendar.newevent

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.calendar.newevent.NewEventItem
import com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.NewEventViewModel
import com.modern.smartsport.ui.common.BaseFragment
    import com.modern.smartsport.ui.common.list.delegate_adapter.calendar.newevent.newEventAdapterDelegate
import kotlinx.android.synthetic.main.fragment_new_event.*
import kotlinx.android.synthetic.main.rv_new_event_item.view.*

class NewEventFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_new_event


    private val viewModel: NewEventViewModel by lazy {
        ViewModelProviders.of(this).get(NewEventViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = ListDelegationAdapter<List<Any>>(
            newEventAdapterDelegate {
                when (it.tvDescription.text) {
                    getString(R.string.fragment_new_event_select_place) -> viewModel.onItemSelectPlaceClick()
                }
            }
        )
        rvEventItem.layoutManager = LinearLayoutManager(requireContext())
        rvEventItem.adapter = adapter
        adapter.items = listItems
    }

    private val listItems: List<NewEventItem> by lazy {
        listOf(
            NewEventItem(R.drawable.ic_ball, getString(R.string.fragment_new_event_sport_type)),
            NewEventItem(R.drawable.ic_time, getString(R.string.fragment_new_event_select_time)),
            NewEventItem(
                R.drawable.ic_geo_marker,
                getString(R.string.fragment_new_event_select_place)
            ),
            NewEventItem(R.drawable.ic_cash, getString(R.string.fragment_new_event_enter_price)),
            NewEventItem(
                R.drawable.ic_event_type,
                getString(R.string.fragment_new_event_enter_event_type)
            ),
            NewEventItem(
                R.drawable.ic_invite,
                getString(R.string.fragment_new_event_invite_members)
            )
        )
    }
}
