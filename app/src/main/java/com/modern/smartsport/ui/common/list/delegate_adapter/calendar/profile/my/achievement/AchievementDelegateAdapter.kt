package com.modern.smartsport.ui.common.list.delegate_adapter.calendar.profile.my.achievement

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.profile.my.achievement.Achievement

fun achievementAdapterDelegate() = adapterDelegate<Achievement, Any>(R.layout.rv_achievement_item) {

    val ivIcon: ImageView = findViewById(R.id.ivIcon)
    val tvTitle: TextView = findViewById(R.id.tvTitle)
    val tvStatus: TextView = findViewById(R.id.tvStatus)
    val tvDescription: TextView = findViewById(R.id.tvDescription)

    bind {
        ivIcon.setImageResource(item.iconId)
        tvTitle.text = item.title
        tvStatus.isVisible = item.isStatus
        tvDescription.text = item.description
    }

}