package com.modern.smartsport.ui.common.list.delegate_adapter.objects

import android.view.View
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.objects.DistanceFilter

fun objectDistanceAdapterDelegate(onAccept: (View) -> Unit, onClear: (View) -> Unit) =
    adapterDelegate<DistanceFilter, Any>(R.layout.rv_distance_filter_item) {

        val seekBar: SeekBar = findViewById(R.id.seekBar)
        val tvCount: TextView = findViewById(R.id.tvCount)
        val btnAccept: Button = findViewById(R.id.btnAccept)
        val btnClear: Button = findViewById(R.id.btnClear)

        bind {
            btnAccept.setOnClickListener {
                onAccept(it)
            }

            btnClear.setOnClickListener {
                onClear(itemView)
            }

            seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    tvCount.text = progress.toString()
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {}
                override fun onStopTrackingTouch(seekBar: SeekBar?) {}

            })

        }
    }