package com.modern.smartsport.ui.fragment.home.plans.traning

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.modern.smartsport.R
import com.modern.smartsport.extension.setLinearDefaultLayoutManager
import com.modern.smartsport.presentation.viewmodel.home.plans.traning.TraningViewModel
import com.modern.smartsport.ui.common.BaseFragment
import com.modern.smartsport.ui.common.list.decorators.addMarginVerticalItemDecorator
import com.modern.smartsport.ui.common.list.delegate_adapter.adExpertAdapterDelegate
import kotlinx.android.synthetic.main.fragment_traning.*
import kotlinx.android.synthetic.main.view_stub_empty_plans.view.*

class TraningFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_traning

    private val viewModel: TraningViewModel by lazy {
        ViewModelProviders.of(this).get(TraningViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ListDelegationAdapter<List<Any>>(
            adExpertAdapterDelegate {
                viewModel.signUpClick(it)
            }
        )

        rvTraningPlan.adapter = adapter
        rvTraningPlan.setLinearDefaultLayoutManager()
        rvTraningPlan.addMarginVerticalItemDecorator(
            topResId = R.dimen.margin_40,
            bottomResId = R.dimen.margin_25
        )

        viewModel.adTrainsExpert.observe(viewLifecycleOwner, Observer {
            adapter.items = it
            adapter.notifyDataSetChanged()
        })

        viewStubPlansEmpty.btnGetProgram.setOnClickListener {
            viewModel.onBtnGetTraningProgramClick()
        }

        viewModel.traningViewModelState.observe(viewLifecycleOwner, Observer {
            when (it) {
                TraningViewModel.State.EmptyTraningProgram -> {
                    viewStubPlansEmpty.isVisible = true
                }

                TraningViewModel.State.NoEmptyTraningProgram -> {
                    viewStubPlansEmpty.isVisible = false

                    rvTraningPlan.isVisible = true
                }
            }
        })
    }
}
