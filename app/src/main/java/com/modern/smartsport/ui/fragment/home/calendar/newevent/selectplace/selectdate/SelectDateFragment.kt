package com.modern.smartsport.ui.fragment.home.calendar.newevent.selectplace.selectdate

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.selectplace.selectdate.SelectDateViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_select_date.*

class SelectDateFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_select_date

    private val viewModel: SelectDateViewModel by lazy {
        ViewModelProviders.of(this).get(SelectDateViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        calendarView.setOnClickListener {
            viewModel.onCalendarViewClick()
        }

        btnOk.setOnClickListener {
            viewModel.onBtnOkClick()
        }

        btnClose.setOnClickListener {
            findNavController().popBackStack()
        }

    }
}
