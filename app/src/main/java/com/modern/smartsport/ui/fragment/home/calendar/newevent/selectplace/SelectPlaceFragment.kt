package com.modern.smartsport.ui.fragment.home.calendar.newevent.selectplace

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.calendar.selectplace.FokDescriptionItem
import com.modern.smartsport.extension.getDimen
import com.modern.smartsport.extension.setLinearDefaultLayoutManager
import com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.selectplace.SelectPlaceViewModel
import com.modern.smartsport.ui.common.BaseFragment
import com.modern.smartsport.ui.common.list.delegate_adapter.calendar.selectplace.descriptionAdapterDelegate
import com.modern.smartsport.ui.common.list.delegate_adapter.calendar.selectplace.timeReservationAdapterDelegate
import kotlinx.android.synthetic.main.bottom_sheet_select_fok.*

class SelectPlaceFragment : BaseFragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    override val layoutResId = R.layout.fragment_select_place

    private val viewModel: SelectPlaceViewModel by lazy {
        ViewModelProviders.of(this).get(SelectPlaceViewModel::class.java)
    }

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ScrollView>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_select_place, container, false)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bottomSheetBehavior = BottomSheetBehavior.from<ScrollView>(bottomSheet)

        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.peekHeight = 0
        bottomSheetBehavior.isHideable = false

        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(p0: View, p1: Float) {
                bottomSheet.scrollTo(0, 0)
            }

            override fun onStateChanged(p0: View, state: Int) {

            }

        })
        tvTitle.text = getString(R.string.fragment_select_place_test_title)
        tvDescription.text = getString(R.string.fragment_select_place_test_description)

        rvTimeReservation.setLinearDefaultLayoutManager()
        val adapterTimeReservation = ListDelegationAdapter<List<String>>(
            timeReservationAdapterDelegate()
        )
        rvTimeReservation.adapter = adapterTimeReservation
        adapterTimeReservation.items = listTimeReservation

        rvDescription.setLinearDefaultLayoutManager()
        val adapterDescription = ListDelegationAdapter<List<FokDescriptionItem>>(
            descriptionAdapterDelegate()
        )
        rvDescription.adapter = adapterDescription
        adapterDescription.items = listDescription

        btnSelectDate.setOnClickListener {
            viewModel.onBtnSelectDateClick()
        }

        btnReservation.setOnClickListener {
            viewModel.onBtnReservationClick()
        }


    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.addMarker(
            MarkerOptions().position(
                LatLng(
                    -34.0,
                    131.0
                )
            ).title("Marker is sydney")
        )
        googleMap.addMarker(
            MarkerOptions().position(
                LatLng(
                    -14.0,
                    151.0
                )
            ).title("Marker is ")
        )
        googleMap.addMarker(MarkerOptions().position(LatLng(-24.0, 111.0)).title("Marker is Kek"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(-34.0, 151.0)))
        googleMap.setOnMarkerClickListener(this)


    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        bottomSheetBehavior.bottomBarPeekUp()
        return false
    }

    /**
     * устанавливает peekHeight bottomSheet в 80dp
     *
     */
    private fun BottomSheetBehavior<ScrollView>.bottomBarPeekUp() {
        this.peekHeight = getDimen(R.dimen.peek_height_bottom_bar_80)
    }

    private val listTimeReservation: List<String> by lazy {
        listOf(
            "Вторник, 24 января, 17:00 - 19:00",
            "Среда, 25 января, 17:00 - 19:00",
            "Четверг, 26 января, 17:00 - 19:00",
            "Пятница, 27 января, 17:00 - 19:00",
            "Суббота, 28 января, 17:00 - 19:00",
            "Воскресенье, 29 января, 17:00 - 19:00",
            "Понедельник, 30 января, 17:00 - 19:00"
        )
    }

    private val listDescription: List<FokDescriptionItem> by lazy {
        listOf(
            FokDescriptionItem(
                getString(R.string.fragment_select_place_adress),
                "Молодежная ул., 35, Волгоград"
            ),
            FokDescriptionItem(
                getString(R.string.fragment_select_place_time),
                "Открыто ⋅ Закроется в 22:00"
            ),
            FokDescriptionItem(getString(R.string.fragment_select_place_phone), "8 (844) 235-82-35")

        )
    }
}
