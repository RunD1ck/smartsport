package com.modern.smartsport.ui.fragment.home.calendar.newevent.selectplace.choosedate

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.selectplace.choosedate.ChooseDateViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_choose_date.*

class ChooseDateFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_choose_date

    private val viewModel: ChooseDateViewModel by lazy {
        ViewModelProviders.of(this).get(ChooseDateViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btnOk.setOnClickListener {
            findNavController().navigate(R.id.action_chooseReservationFragment_to_selectDateFragment)
        }

        btnClose.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}
