package com.modern.smartsport.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.modern.smartsport.R
import com.modern.smartsport.di.globalNavScopeId
import com.modern.smartsport.extension.translucentToolbar
import com.modern.smartsport.extension.visibleToolbar
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.getKoin


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        window.decorView.systemUiVisibility = 0

        val idsFragmentsWithHideToolbar = listOf(
            R.id.loginFragment,
            R.id.phoneLoginFragment,
            R.id.faceLoginFragment,
            R.id.checkSmsCodeFragment
        )

        setSupportActionBar(toolbar)

        mainActivityNavHostFragment.findNavController()
            .addOnDestinationChangedListener { controller, destination, arguments ->

                if (destination.id != R.id.homeFragment) {
                    NavigationUI.setupWithNavController(toolbar, controller)
                }

                visibleToolbar(!idsFragmentsWithHideToolbar.contains(destination.id))
                translucentToolbar()
            }

        toolbar.setOnApplyWindowInsetsListener { view, insets ->
            val newLayoutParams =
                toolbar.layoutParams as ConstraintLayout.LayoutParams
            newLayoutParams.topMargin = insets.systemWindowInsetTop
            toolbar.layoutParams = newLayoutParams
            insets
        }
    }

    override fun onStop() {
        getKoin().deleteScope(globalNavScopeId)
        super.onStop()
    }

    override fun onBackPressed() {
        if (mainActivityNavHostFragment.findNavController().currentDestination?.id == R.id.homeFragment) {
            if ((mainActivityNavHostFragment.childFragmentManager.fragments[0] as BaseFragment).onBackPressed()) {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }
}
