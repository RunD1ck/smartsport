package com.modern.smartsport.ui.fragment.auth.phone

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.extension.setMask
import com.modern.smartsport.presentation.viewmodel.auth.PhoneLoginViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_phone_login.*


class PhoneLoginFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_phone_login

    private val viewModel: PhoneLoginViewModel by lazy {
        ViewModelProviders.of(this).get(PhoneLoginViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnSendCode.setOnClickListener {
            viewModel.onBtnSendClick(etPhoneNumber.text.toString())
        }

        tvClose.setOnClickListener {
            viewModel.onTvCloseClick()
        }

        etPhoneNumber.setMask(getString(R.string.mask)) {

        }
    }
}
