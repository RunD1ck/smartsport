package com.modern.smartsport.ui.fragment.home.events

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.events.EventsViewModel
import com.modern.smartsport.ui.common.BaseFragment

class EventsFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_events

    private val viewModel: EventsViewModel by lazy {
        ViewModelProviders.of(this).get(EventsViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}
