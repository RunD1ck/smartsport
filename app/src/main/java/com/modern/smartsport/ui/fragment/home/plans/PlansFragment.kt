package com.modern.smartsport.ui.fragment.home.plans

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.plans.main.Plan
import com.modern.smartsport.extension.setLinearDefaultLayoutManager
import com.modern.smartsport.presentation.viewmodel.home.plans.PlansViewModel
import com.modern.smartsport.ui.common.BaseFragment
import com.modern.smartsport.ui.common.list.decorators.addMarginVerticalItemDecorator
import com.modern.smartsport.ui.common.list.delegate_adapter.planAdapterDelegate
import kotlinx.android.synthetic.main.fragment_plans.*

class PlansFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_plans

    private val viewModel: PlansViewModel by lazy {
        ViewModelProviders.of(this).get(PlansViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPlans({
            viewModel.onTraningPlanClick()
        }, {
            viewModel.onNutritionClick()
        })
    }

    /**
     * Рисует на UI 2 элемента плана "План тренировок" и "План питания"
     * @param onTraningClick - обработчик нажатия на элемент "План тренировок"
     * @param onNutritionClick - обработчик нажатия на элемент "План питания"
     */
    private fun initPlans(onTraningClick: () -> Unit, onNutritionClick: () -> Unit) {
        val adapter = ListDelegationAdapter<List<Any>>(
            planAdapterDelegate { pos ->
                when (pos) {
                    0 -> onTraningClick()
                    1 -> onNutritionClick()
                }
            }
        )

        rvPlans.adapter = adapter
        rvPlans.setLinearDefaultLayoutManager()
        rvPlans.addMarginVerticalItemDecorator(R.dimen.margin_20)

        adapter.items =
            listOf(
                Plan(
                    getString(R.string.traning_plan),
                    getString(R.string.train_subtitle),
                    "2 Августа, 12:00 - 13:00, Кардио", //TODO должно генерироваться системой
                    R.drawable.background_traning_plan
                ),
                Plan(
                    getString(R.string.nutrition_plan),
                    getString(R.string.train_subtitle),
                    "Завтрак, 8:30 - 9:00, Молочная овсяная каша\n" + //TODO должно генерироваться системой
                            "с изюмом или курагой, 2 яйца всмятку, молоко.",
                    R.drawable.background_nutrition_plan
                )
            )
    }
}
