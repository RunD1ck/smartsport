package com.modern.smartsport.ui.common.pagination

import android.util.Log
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.modern.smartsport.entity.common.pagination.PaginationDTO
import com.modern.smartsport.entity.home.calendar.BaseCalendar
import com.modern.smartsport.model.interactor.home.calendar.CalendarInteractor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CalendarDataSource(
    private val coroutineScope: CoroutineScope,
    private val calendarInteractor: CalendarInteractor,
    pageSize: String
) : PageKeyedDataSource<Long, BaseCalendar>() {

    private val paginationDTO =
        PaginationDTO(
            "0",
            pageSize
        )

    override fun loadInitial(
        params: LoadInitialParams<Long>,
        callback: LoadInitialCallback<Long, BaseCalendar>
    ) {
        coroutineScope.launch {
            try {
                val calendar = calendarInteractor.getCalendar(
                    paginationDTO
                )

                withContext(Dispatchers.Main) {
                    callback.onResult(calendar, null, 1)
                }
            } catch (e: Exception) {
                Log.e("Paging error", e.message.toString())
            }
        }
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, BaseCalendar>) {
        coroutineScope.launch {
            try {
                val calendar = calendarInteractor.getCalendar(
                    paginationDTO.apply {
                        offset = params.key.toString()
                    }
                )

                withContext(Dispatchers.Main) {
                    callback.onResult(calendar, (params.key + 1))
                }
            } catch (e: Exception) {
                Log.e("Paging error", e.message.toString())
            }
        }
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, BaseCalendar>) {

    }

    class Factory(
        private val coroutineScope: CoroutineScope,
        private val feedInteractor: CalendarInteractor,
        private val pageSize: String
    ) : DataSource.Factory<Long, BaseCalendar>() {
        override fun create(): DataSource<Long, BaseCalendar> {
            return CalendarDataSource(coroutineScope, feedInteractor, pageSize)
        }
    }
}