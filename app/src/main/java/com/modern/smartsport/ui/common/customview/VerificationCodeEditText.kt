package com.modern.smartsport.ui.common.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.util.AttributeSet
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText

class VerificationCodeEditText : EditText { //TODO:Сделать нормальную кастомную вью

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0) {
        setBackgroundResource(0)
        val multi = context.resources.displayMetrics.density
        space *= multi
        radius *= multi
        lineSpacing *= multi

        super.setCustomSelectionActionModeCallback(
            object : ActionMode.Callback {
                override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?) = false
                override fun onCreateActionMode(mode: ActionMode?, menu: Menu?) = false
                override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?) = false
                override fun onDestroyActionMode(mode: ActionMode?) {}

            })
        //When tapped, move cursor to end of the text
        super.setOnClickListener { clickListener?.onClick(it) }
    }


    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    var space = 40f //24 dp by default

    var charSize = 0f
    var numChars = 4f
    var radius = 10f
    var lineSpacing = 8f
    val maxLength = 4
    var clickListener: OnClickListener? = null

    override fun onDraw(canvas: Canvas?) {
        val availableWidth = (width - paddingRight - paddingLeft).toFloat()
        charSize = if (space < 0) {
            availableWidth / (numChars * 2 - 1)
        } else {
            (availableWidth - space * (numChars - 1)) / numChars
        }

        var startX = 0f
        val bottom = (height - paddingBottom).toFloat()
        val textWidths = FloatArray(text.length)
        paint.getTextWidths(text, 0, text.length, textWidths)

        for (i in 0 until numChars.toInt()) {
            paint.color = Color.WHITE
            canvas?.drawRoundRect(
                startX,
                paddingTop + paddingBottom + charSize,
                startX + paddingEnd + paddingStart + charSize,
                0f,
                radius,
                radius, paint
            )
            paint.color = Color.BLACK
            if (text.length > i) {
                val middle: Float = startX + charSize / 2 + paddingStart / 2
                canvas?.drawText(
                    text,
                    i,
                    i + 1,
                    middle - textWidths[0] / 2 + paddingStart / 2,
                    paddingTop  + charSize,
                    paint
                )
            }
            startX += (charSize + space).toInt()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        var measuredWidth = 0
        var measuredHeight = 0
        // If we want a square or circle pin box, we might be able
        // to figure out the dimensions outselves
        // if width and height are set to wrap_content or match_parent
        // If we want a square or circle pin box, we might be able
// to figure out the dimensions outselves
// if width and height are set to wrap_content or match_parent
        if (widthMode == MeasureSpec.EXACTLY) {
            measuredWidth = MeasureSpec.getSize(widthMeasureSpec)
            measuredHeight = ((measuredWidth - (numChars - 1 * space)) / numChars).toInt()
        } else if (heightMode == MeasureSpec.EXACTLY) {
            measuredHeight = MeasureSpec.getSize(heightMeasureSpec)
            measuredWidth = (measuredHeight * numChars + (space * numChars - 1)).toInt()
        } else if (widthMode == MeasureSpec.AT_MOST) {
            measuredWidth = MeasureSpec.getSize(widthMeasureSpec)
            measuredHeight = ((measuredWidth - (numChars - 1 * space)) / numChars).toInt()
        } else if (heightMode == MeasureSpec.AT_MOST) {
            measuredHeight = MeasureSpec.getSize(heightMeasureSpec)
            measuredWidth = (measuredHeight * numChars + (space * numChars - 1)).toInt()
        } else { // Both unspecific
// Try for a width based on our minimum
            measuredWidth = paddingStart + paddingEnd + suggestedMinimumWidth
            // Whatever the width ends up being, ask for a height that would let the pie
// get as big as it can
            measuredHeight = ((measuredWidth - (numChars - 1 * space)) / numChars).toInt()
        }

        setMeasuredDimension(
            View.resolveSizeAndState(measuredWidth, widthMeasureSpec, 1),
            View.resolveSizeAndState(measuredHeight, heightMeasureSpec, 0)
        )

    }

    override fun setOnClickListener(l: OnClickListener?) {
        clickListener = l
    }

}
