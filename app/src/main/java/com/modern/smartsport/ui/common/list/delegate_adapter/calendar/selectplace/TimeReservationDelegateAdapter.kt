package com.modern.smartsport.ui.common.list.delegate_adapter.calendar.selectplace

import android.widget.TextView
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R

fun timeReservationAdapterDelegate() =
    adapterDelegate<String, String>(R.layout.rv_time_reservation_item) {

        val tvTimeReservation: TextView = findViewById(R.id.tvTimeReservation)

        bind {
            tvTimeReservation.text = item
        }

    }