package com.modern.smartsport.ui.fragment.home.calendar.newevent.selectplace.reservation

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.selectplace.reservation.ReservationViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_reservation.*

class ReservationFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_reservation

    private val viewModel: ReservationViewModel by lazy {
        ViewModelProviders.of(this).get(ReservationViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnReservation.setOnClickListener {
            findNavController().navigate(R.id.action_reservationFragment_to_successReservationFragment)
        }

    }
}
