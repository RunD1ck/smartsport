package com.modern.smartsport.ui.fragment.auth.login

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.auth.login.LoginViewModel
import com.modern.smartsport.ui.common.BaseFragment
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_login

    private val viewModel: LoginViewModel by lazy {
        ViewModelProviders.of(this).get(LoginViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnFaceLogin.setOnClickListener {
            viewModel.onLoginFaceBtnClick()
        }

        btnLogin.setOnClickListener {
            viewModel.onLoginPhoneBtnClick()
        }

        tvSignInWithoutLogin.setOnClickListener {
            viewModel.onLoginWithoutRegistrationBtnClick()
        }
    }
}
