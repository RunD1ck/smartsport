package com.modern.smartsport.ui.fragment.home.objects

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.objects.DistanceFilter
import com.modern.smartsport.entity.home.objects.ObjectFilter
import com.modern.smartsport.extension.setLinearDefaultLayoutManager
import com.modern.smartsport.presentation.viewmodel.home.objects.ObjectsViewModel
import com.modern.smartsport.ui.common.BaseFragment
import com.modern.smartsport.ui.common.list.decorators.addMarginHorizontalItemDecorator
import com.modern.smartsport.ui.common.list.delegate_adapter.ObjectFilterPayload
import com.modern.smartsport.ui.common.list.delegate_adapter.objectFilterDelegateAdapter
import kotlinx.android.synthetic.main.fragment_objects.*
import kotlinx.android.synthetic.main.view_search.*
import kotlinx.android.synthetic.main.view_search.view.*

class ObjectsFragment : BaseFragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    override val layoutResId = R.layout.fragment_objects

    private val viewModel: ObjectsViewModel by lazy {
        ViewModelProviders.of(this).get(ObjectsViewModel::class.java)
    }

    private val objectBottomSheetFragment: ObjectBottomSheetFragment by lazy {
        ObjectBottomSheetFragment()
    }


    private lateinit var googleMap: GoogleMap

    private val Kazan = LatLng(
        55.822117, 49.113681
    )

    private val defaultZoom = 12f


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_objects, container, false)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        return rootView
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Kazan, defaultZoom))
        googleMap.addMarker(
            MarkerOptions()
                .position(Kazan)
                .icon(
                    BitmapDescriptorFactory.fromResource(R.drawable.ic_fok_marker)
                )
        )
        googleMap.setOnMarkerClickListener(this)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnChangeState.setOnClickListener {
            viewModel.onToggleStateClick()
        }

        viewModel.objectInfo.observe(viewLifecycleOwner, Observer {
            showBottomSheet(listOf(it))
        })

        viewModel.state.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                ObjectsViewModel.State.ShowMap -> {
                    findMapFragment()?.let {
                        childFragmentManager.beginTransaction().show(it).commit()
                        rvObjects.isVisible = false
                        btnChangeState.text = getString(R.string.list)
                    }
                }
                ObjectsViewModel.State.ShowList -> {
                    findMapFragment()?.let {
                        childFragmentManager.beginTransaction().hide(it).commit()
                        rvObjects.isVisible = true
                        btnChangeState.text = getString(R.string.map)
                    }
                }
            }
        })

        var curSelectPos = 0

        val items = listOf(
            ObjectFilter("Все категории"), //TODO инфа только для теста в будущем будет изменена
            ObjectFilter("Услуги"),
            ObjectFilter("Расстояние")
        )

        searchView.setOnClickListener {
            searchView.svSearch.onActionViewExpanded()
        }

        rvObjectFilter.setLinearDefaultLayoutManager(orientation = RecyclerView.HORIZONTAL)

        fun selectObjectFilterState(pos: Int) {
            rvObjectFilter.adapter?.notifyItemChanged(
                pos,
                ObjectFilterPayload.SELECTED
            )

            rvObjectFilter.adapter?.notifyItemChanged(
                curSelectPos,
                ObjectFilterPayload.NO_SELECTED
            )
            curSelectPos = pos

            when (items[curSelectPos].name) {
                "Все категории" -> {
                    showBottomSheet()
                }
                "Услуги" -> {
                    if (viewModel.listServices.value == null) {
                        viewModel.listServices.observe(viewLifecycleOwner, Observer {
                            showBottomSheet(it)
                        })
                    } else {
                        showBottomSheet(
                            viewModel.listServices.value ?: listOf()
                        )
                    }
                }
                "Расстояние" -> {
                    showBottomSheet(listOf(DistanceFilter()))
                }
            }

        }

        val rvAdapter = ListDelegationAdapter<List<Any>>(
            objectFilterDelegateAdapter {
                selectObjectFilterState(it)
            }
        )

        rvAdapter.items = items
        rvObjectFilter.adapter = rvAdapter
        rvObjectFilter.addMarginHorizontalItemDecorator(
            leftResId = R.dimen.margin_15,
            topResId = R.dimen.margin_10,
            rightResId = R.dimen.margin_7,
            bottomResId = R.dimen.margin_10
        )
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        viewModel.onMarkerClick(marker.position.latitude, marker.position.longitude)
        return false
    }

    private fun showBottomSheet(list: List<Any>? = null) {
        if (!objectBottomSheetFragment.isAdded) {
            objectBottomSheetFragment.show(childFragmentManager, objectBottomSheetFragment.tag)
        } else {
            objectBottomSheetFragment.dismiss()
        }
        if (list != null) {
            objectBottomSheetFragment.setList(list)
        } else {
            objectBottomSheetFragment.setList()
        }
    }

    /**
     * Находит фрагмент с картой
     *
     */
    private fun findMapFragment() = childFragmentManager.findFragmentById(R.id.map)

}
