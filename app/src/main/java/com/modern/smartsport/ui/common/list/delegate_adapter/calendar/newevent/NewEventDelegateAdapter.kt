package com.modern.smartsport.ui.common.list.delegate_adapter.calendar.newevent

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.calendar.newevent.NewEventItem

fun newEventAdapterDelegate(clickListener: (View) -> Unit) =
    adapterDelegate<NewEventItem, Any>(R.layout.rv_new_event_item) {
        val ivIcon: ImageView = findViewById(R.id.ivIcon)
        val tvDescription: TextView = findViewById(R.id.tvDescription)

        itemView.setOnClickListener {
            clickListener(it)
        }
        bind {
            ivIcon.setImageResource(item.iconId)
            tvDescription.text = item.description
        }
    }