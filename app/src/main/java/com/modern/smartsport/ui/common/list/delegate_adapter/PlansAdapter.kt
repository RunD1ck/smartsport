package com.modern.smartsport.ui.common.list.delegate_adapter

import android.widget.ImageView
import android.widget.TextView
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.plans.main.Plan

fun planAdapterDelegate(itemClickedListener: (Int) -> Unit) =
    adapterDelegate<Plan, Any>(R.layout.item_plans) {

        val name: TextView = findViewById(R.id.tvTitle)
        val tvSubTitle: TextView = findViewById(R.id.tvSubTitle)
        val message: TextView = findViewById(R.id.tvMessage)
        val imageBg: ImageView = findViewById(R.id.ivBackground)

        itemView.setOnClickListener { itemClickedListener(adapterPosition) }

        bind { diffPayloads ->

            name.text = item.name
            tvSubTitle.text = item.subTitle
            message.text = item.message
            imageBg.setImageResource(item.backgroundResId)
        }

    }