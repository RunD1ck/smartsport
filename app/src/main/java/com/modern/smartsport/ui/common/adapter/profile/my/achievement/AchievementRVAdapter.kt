package com.modern.smartsport.ui.common.adapter.profile.my.achievement

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.modern.smartsport.entity.home.profile.my.achievement.Achievement
import com.modern.smartsport.ui.common.list.delegate_adapter.calendar.profile.my.achievement.achievementAdapterDelegate


fun Achievement.isSame(achievement: Achievement) = this.id == achievement.id

class AchievementRVAdapter :
    AsyncListDifferDelegationAdapter<Any>(object : DiffUtil.ItemCallback<Any>() {
        override fun areItemsTheSame(oldItem: Any, newItem: Any) =
            (oldItem as Achievement).isSame(newItem as Achievement)

        override fun areContentsTheSame(oldItem: Any, newItem: Any) =
            (oldItem as Achievement).isSame(newItem as Achievement)

        override fun getChangePayload(oldItem: Any, newItem: Any) = Any()

    }) {
    init {
        items = mutableListOf()
        delegatesManager.addDelegate(achievementAdapterDelegate())
    }

}