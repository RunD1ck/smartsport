package com.modern.smartsport.ui.fragment.home.profile.my.achievement

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.profile.my.achievement.Achievement
import com.modern.smartsport.extension.setLinearDefaultLayoutManager
import com.modern.smartsport.presentation.viewmodel.home.profile.my.achievement.AchievementViewModel
import com.modern.smartsport.ui.common.BaseFragment
import com.modern.smartsport.ui.common.adapter.profile.my.achievement.AchievementRVAdapter
import kotlinx.android.synthetic.main.fragment_achievement.*

class AchievementFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_achievement

    private val achievementRVAdapter: AchievementRVAdapter by lazy {
        AchievementRVAdapter()
    }

    private val viewModel: AchievementViewModel by lazy {
        ViewModelProviders.of(this).get(AchievementViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvAchievement.setLinearDefaultLayoutManager()
        rvAchievement.adapter = achievementRVAdapter

        viewModel.listAchievements.observe(viewLifecycleOwner, Observer<List<Achievement>> {
            achievementRVAdapter.items = it
        })
    }
}
