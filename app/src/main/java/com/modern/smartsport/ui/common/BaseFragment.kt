package com.modern.smartsport.ui.common

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.di.globalNavScopeId
import com.modern.smartsport.di.navScopeScopeName
import com.modern.smartsport.ui.MainActivity
import com.modern.smartsport.ui.navigation.NavControllerNavigator
import org.koin.android.ext.android.getKoin
import org.koin.core.qualifier.named

abstract class BaseFragment : Fragment() {

    abstract val layoutResId: Int?
    open var menuResId: Int? = null

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menuResId?.let {
            inflater.inflate(it, menu)
            super.onCreateOptionsMenu(menu, inflater)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //Обновляем ссылку на глобальный navController ,потому что
        //она может иногда изменяться
        val navControllerNavigator = getKoin().getOrCreateScope(
            globalNavScopeId, named(
                navScopeScopeName
            )
        ).get<NavControllerNavigator>()

        navControllerNavigator.navController =
            (activity as MainActivity).findNavController(R.id.mainActivityNavHostFragment)

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return layoutResId?.let { inflater.inflate(it, container, false) }
    }

    /**
     * @return true if activity need handle on onBackPressed() callback
     */
    open fun onBackPressed(): Boolean {
        return false
    }
}