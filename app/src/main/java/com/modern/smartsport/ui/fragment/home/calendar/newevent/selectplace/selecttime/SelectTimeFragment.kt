package com.modern.smartsport.ui.fragment.home.calendar.newevent.selectplace.selecttime

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.selectplace.selecttime.SelectTimeViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_select_time.*

class SelectTimeFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_select_time

    private val viewModel: SelectTimeViewModel by lazy {
        ViewModelProviders.of(this).get(SelectTimeViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnOk.setOnClickListener {
            findNavController().navigate(R.id.action_selectTimeFragment_to_successReservationFragment)
        }

        btnClose.setOnClickListener {
            findNavController().popBackStack()
        }

    }
}
