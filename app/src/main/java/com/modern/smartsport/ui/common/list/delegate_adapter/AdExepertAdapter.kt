package com.modern.smartsport.ui.common.list.delegate_adapter

import android.widget.ImageView
import android.widget.TextView
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.modern.smartsport.R
import com.modern.smartsport.entity.home.plans.my.AdExpertProfile

fun adExpertAdapterDelegate(itemClickedListener: (AdExpertProfile) -> Unit) =
    adapterDelegate<AdExpertProfile, Any>(R.layout.item_ad_expert) {

        val expertPhoto: ImageView = findViewById(R.id.ivExpertPhoto)
        val nameAndSurname: TextView = findViewById(R.id.tvNameAndSurname)
        val bio: TextView = findViewById(R.id.tvBio)
        val tvSignUp: TextView = findViewById(R.id.tvSignUp)

        tvSignUp.setOnClickListener {
            itemClickedListener(item)
        }

        bind { diffPayloads ->
            expertPhoto.setImageResource(item.avatarResId)
            nameAndSurname.text = item.nameAndSurname
            bio.text = item.bio
        }
    }