package com.modern.smartsport.ui.fragment.home.plans.nutrition

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.plans.nutrition.NutritionViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_nutrition.*
import kotlinx.android.synthetic.main.view_stub_empty_plans.*

class NutritionFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_nutrition

    private val viewModel: NutritionViewModel by lazy {
        ViewModelProviders.of(this).get(NutritionViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initStubEmptyNutritionPlan()

        viewModel.nutritionViewModelState.observe(viewLifecycleOwner, Observer {
            when (it) {
                NutritionViewModel.State.EmptyTraningProgram -> {
                    viewStubPlansEmpty.isVisible = true
                }
                NutritionViewModel.State.NoEmptyTraningProgram -> {
                    viewStubPlansEmpty.isVisible = false
                }

            }
        })
    }

    /**
     * Заполняем текстом заглушку пустого плана питания
     *
     */
    private fun initStubEmptyNutritionPlan() {
        ivPlanIcon.setImageResource(R.drawable.ic_food)
        tvEmptyProgram.text = getString(R.string.empty_nutrition_plan)
        btnGetProgram.text = getString(R.string.get_nutrition_plan)
    }
}
