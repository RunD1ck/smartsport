package com.modern.smartsport.ui.fragment.home.profile.comments

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.profile.comments.AllCommentsViewModel
import com.modern.smartsport.ui.common.BaseFragment

class AllCommentsFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_all_comments

    private val viewModel: AllCommentsViewModel by lazy {
        ViewModelProviders.of(this).get(AllCommentsViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}
