package com.modern.smartsport.ui.fragment.home

import android.os.Bundle
import android.view.View
import androidx.core.view.updatePadding
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.ui.NavigationUI
import com.modern.smartsport.R
import com.modern.smartsport.di.getNavScopeName
import com.modern.smartsport.di.navScopeScopeName
import com.modern.smartsport.entity.home.plans.my.AdExpertProfile
import com.modern.smartsport.extension.removePaddingFromNavigationItem
import com.modern.smartsport.extension.setTitle
import com.modern.smartsport.extension.setupWithNavController
import com.modern.smartsport.extension.translucentToolbar
import com.modern.smartsport.ui.MainActivity
import com.modern.smartsport.ui.common.BaseFragment
import com.modern.smartsport.ui.navigation.NavControllerNavigator
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.getKoin
import org.koin.core.qualifier.named
import java.util.*

class HomeFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_home

    private var currentNavController: LiveData<NavController>? = null

    private var navScopesIds: HashSet<String> = hashSetOf()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        }

        llHome.setOnApplyWindowInsetsListener { llHome, insets ->
            llHome.updatePadding(top = insets.systemWindowInsetTop)
            return@setOnApplyWindowInsetsListener insets
        }

        bottomNavigationView.setOnApplyWindowInsetsListener { view, insets ->
            view.updatePadding(bottom = insets.systemWindowInsetBottom)
            insets
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        setupBottomNavigationBar()
    }

    private fun setupBottomNavigationBar() {
        bottomNavigationView.removePaddingFromNavigationItem()

        val navGraphIds = listOf(
            R.navigation.events,
            R.navigation.objects,
            R.navigation.calendar,
            R.navigation.plans,
            R.navigation.profile
        )

        val controller = bottomNavigationView.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = childFragmentManager,
            containerId = R.id.homeNavHostContainer,
            intent = requireActivity().intent
        )

        controller.observe(viewLifecycleOwner, Observer { navController ->

            NavigationUI.setupWithNavController((activity as MainActivity).toolbar, navController)

            currentNavController?.value?.addOnDestinationChangedListener { controller, destination, arguments ->

                val navScopeId = getNavScopeName(controller.graph.label ?: "")

                val navigatorScope =
                    getKoin().getOrCreateScope(navScopeId, named(navScopeScopeName))
                navigatorScope.get<NavControllerNavigator>().navController = controller
                navScopesIds.add(navScopeId)

                translucentToolbar(destination.id == R.id.profileFragment)

                if (destination.id == R.id.profileExpertFragment) {
                    setTitle(
                        arguments?.getParcelable<AdExpertProfile>("adProfileExpert")?.nameAndSurname
                            ?: ""
                    )
                } else {
                    setTitle(destination.label ?: "")
                }
            }

        })

        currentNavController = controller
    }

    override fun onDestroy() {

        navScopesIds.forEach {
            getKoin().deleteScope(it)
        }

        super.onDestroy()
    }

    override fun onBackPressed(): Boolean {
        val needActivityHandleBackPressed = currentNavController?.value?.navigateUp() ?: true
        return !needActivityHandleBackPressed
    }
}
