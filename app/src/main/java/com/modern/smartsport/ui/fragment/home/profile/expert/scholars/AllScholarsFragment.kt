package com.modern.smartsport.ui.fragment.home.profile.expert.scholars

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.profile.expert.scholars.AllScholarsViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_all_scholar.*

class AllScholarsFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_all_scholar

    private val viewModel: AllScholarsViewModel by lazy {
        ViewModelProviders.of(this).get(AllScholarsViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnBack.setOnClickListener {
            findNavController().popBackStack()
        }

    }
}
