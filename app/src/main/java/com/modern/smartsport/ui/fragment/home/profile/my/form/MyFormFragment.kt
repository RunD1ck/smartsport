package com.modern.smartsport.ui.fragment.home.profile.my.form

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.modern.smartsport.R
import com.modern.smartsport.presentation.viewmodel.home.profile.my.form.MyFormViewModel
import com.modern.smartsport.ui.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_my_form.*

class MyFormFragment : BaseFragment() {

    override val layoutResId = R.layout.fragment_my_form

    private val viewModel: MyFormViewModel by lazy {
        ViewModelProviders.of(this).get(MyFormViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnBack.setOnClickListener {
            findNavController().popBackStack()
        }

    }
}
