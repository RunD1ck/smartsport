package com.modern.smartsport.model.interactor.home.plans.traning

import com.modern.smartsport.R
import com.modern.smartsport.entity.home.plans.my.AdExpertProfile

class TraningInteractor {
    /**
     * Получение рекомендации по экспертам
     *
     */
    fun getAdExpert() = listOf(
        AdExpertProfile(
            "Игорь Егоров",
            "Мастер спорта, КМС по легкой атлетике. Поможет составить Вам идеальный план тренировок.",
            R.drawable.image_expert_avatar
        ), AdExpertProfile(
            "Максим Королев",
            "Мастер спорта, КМС по легкой атлетике. Поможет составить Вам идеальный план тренировок.",
            R.drawable.image_expert_avatar
        )
    )
}
