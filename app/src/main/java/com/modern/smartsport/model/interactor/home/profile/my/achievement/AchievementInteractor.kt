package com.modern.smartsport.model.interactor.home.profile.my.achievement

import com.modern.smartsport.R
import com.modern.smartsport.entity.home.profile.my.achievement.Achievement
import kotlinx.coroutines.delay

class AchievementInteractor {

    private val listAchievements = listOf(
        Achievement(
            1,
            R.drawable.ic_girl,
            "Заряженный",
            true,
            "Держи отметку активности на 100% в течении 3 дней подряд!"
        ),
        Achievement(
            2,
            R.drawable.ic_dumbbell,
            "Новичок",
            false,
            "Начни пользоваться приложением"
        ),
        Achievement(
            3,
            R.drawable.ic_foot,
            "Спринтер",
            false,
            "Пробеги 1 000 км"
        ),
        Achievement(
            4,
            R.drawable.ic_dolphin,
            "Борец-пловец",
            false,
            "Занимайся борьбой и плаванием одновременно"
        ),
        Achievement(
            5,
            R.drawable.ic_heart,
            "Любимчик",
            false,
            "Получи рейтинг 4 звезды"
        ),
        Achievement(
            6,
            R.drawable.ic_heart,
            "Обожаемый",
            false,
            "Получи рейтинг 5 звезд"
        )

    )

    suspend fun getAchievements(): List<Achievement> {
        return listAchievements
    }
}
