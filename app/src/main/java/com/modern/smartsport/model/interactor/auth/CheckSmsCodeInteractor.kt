package com.modern.smartsport.model.interactor.auth

import timber.log.Timber

class CheckSmsCodeInteractor {

    companion object {
        const val TEST_CODE = "1111"
    }

    suspend fun sendCode(code: String): Boolean {
        Timber.v(code)
        return code == TEST_CODE //TODO:Должно приходить с бэка
    }
}
