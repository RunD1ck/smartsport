package com.modern.smartsport.model.interactor.home.calendar

import com.modern.smartsport.entity.common.pagination.PaginationDTO
import com.modern.smartsport.entity.home.calendar.*
import com.modern.smartsport.extension.convertCurrentTimeToDate
import com.modern.smartsport.extension.getChooseMonthDay
import com.modern.smartsport.extension.getChooseWeekDay
import com.modern.smartsport.extension.isIncludeInRange
import java.util.*
import kotlin.collections.ArrayList

class CalendarInteractor {
    private val calendar: Calendar by lazy {
        Calendar.getInstance()
    }

    private val listCalendar = listOf(
        GetCalendarDTO(
            PaginationDTO("0", "2")
            , listOf(
                CalendarDay(
                    Calendar.getInstance().getChooseWeekDay(-1),
                    Calendar.getInstance().getChooseMonthDay(-1),
                    arrayListOf(
                        CalendarEvent(
                            1,
                            "Завтрак",
                            "8:30"
                            , "9:00",
                            "Молочная овсяная каша с изюмом или курагой, 2 яйца всмятку, молоко."
                        ),
                        CalendarEvent(
                            2,
                            "Фортуна - Надежда",
                            "15:00",
                            "17:00",
                            "ФОК Спортивный",
                            CalendarEventType.RESERVATION
                        )
                    )
                ),
                CalendarDay(
                    Calendar.getInstance().getChooseWeekDay(),
                    Calendar.getInstance().getChooseMonthDay(),
                    arrayListOf(
                        CalendarEvent(
                            3,
                            "Силовая тренировка",
                            "8:00",
                            "19:00",
                            "ФОК Молоджный",
                            CalendarEventType.TRANING
                        ),
                        CalendarEvent(
                            4,
                            "Ужин",
                            "19:00",
                            "23:00",
                            "Овощное ассорти, куриная отбивная, кефир или молоко.",
                            CalendarEventType.FOOD
                        )
                    )
                )
            )
        )
    )


    suspend fun getCalendar(paginationDTO: PaginationDTO): List<BaseCalendar> {
        val listCalendarItem = listCalendar[paginationDTO.offset.toInt()].calendarDays

        val listEvents = arrayListOf<BaseCalendar>()

        listCalendarItem.map {
            it.isCurrentTime =
                calendar.getChooseMonthDay() == it.monthDay && calendar.getChooseWeekDay() == it.weekDay
        }

        val currentItem = listCalendarItem.firstOrNull {
            it.isCurrentTime
        }

        val arrayList = arrayListOf<BaseCalendar>()
        arrayList.addAll(currentItem?.listEvents ?: ArrayList())
        currentItem?.listEvents?.forEachIndexed { index, item ->
            val event = item as CalendarEvent

            if (calendar.isIncludeInRange(
                    calendar.convertCurrentTimeToDate(event.timeFrom),
                    calendar.convertCurrentTimeToDate(event.timeTo)
                )
            ) {
                arrayList.add(
                    index + 1,
                    CalendarLineCurrentItem()
                )
            }
        }
        currentItem?.listEvents?.clear()
        currentItem?.listEvents?.addAll(arrayList)

        listCalendarItem.forEach { calendarDay ->
            calendarDay.listEvents.firstOrNull {
                it is CalendarEvent
            }?.let {
                with(it as CalendarEvent) {
                    monthDay = calendarDay.monthDay
                    weekDay = calendarDay.weekDay
                    isCurrentTime = calendarDay.isCurrentTime
                }
            }
            listEvents.addAll(calendarDay.listEvents)
        }
        return listEvents

    }
}
