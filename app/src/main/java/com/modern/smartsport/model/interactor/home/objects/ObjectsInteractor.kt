package com.modern.smartsport.model.interactor.home.objects

import com.modern.smartsport.entity.home.objects.BottomSheetButtons
import com.modern.smartsport.entity.home.objects.ObjectInfoItem
import com.modern.smartsport.entity.home.objects.ServiceFilter

class ObjectsInteractor {

    suspend fun getListServices() = listOf(
        ServiceFilter("Йога"),
        ServiceFilter("Кардио"),
        ServiceFilter("Силовые"),
        ServiceFilter("Единоборства"),
        ServiceFilter("Боевые искусства"),
        ServiceFilter("Плавание"),
        ServiceFilter("Бег"),
        ServiceFilter("Велосипед"),
        ServiceFilter("Пилатес"),
        ServiceFilter("Танцы"),
        BottomSheetButtons()

    )

    suspend fun getObjectInfo(lat: Double, lon: Double) =
        ObjectInfoItem(
            "Плавание",
            "ФОК Водный",
            "ул. Винокурова, 14, Москва, 117447",
            4f,
            34,
            "1ч. 25м.",
            "35м.",
            "55м."

        )

}
