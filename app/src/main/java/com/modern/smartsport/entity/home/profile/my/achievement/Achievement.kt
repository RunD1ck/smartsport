package com.modern.smartsport.entity.home.profile.my.achievement

data class Achievement(
    val id:Int,
    val iconId: Int,
    val title: String,
    val isStatus: Boolean,
    val description: String
)