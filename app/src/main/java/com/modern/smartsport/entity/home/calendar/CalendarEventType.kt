package com.modern.smartsport.entity.home.calendar

import com.modern.smartsport.R

enum class CalendarEventType(val backgroundRes: Int) {
    FOOD(R.drawable.bg_calendar_item_food),
    RESERVATION(R.drawable.bg_calendar_item_reservation),
    TRANING(R.drawable.bg_calendar_item_traning),
    EXPIRE(R.drawable.bg_calendar_item_expire)
}