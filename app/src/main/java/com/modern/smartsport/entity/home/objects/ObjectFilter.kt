package com.modern.smartsport.entity.home.objects

data class ObjectFilter(val name: String)