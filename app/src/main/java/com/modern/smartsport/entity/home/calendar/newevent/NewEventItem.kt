package com.modern.smartsport.entity.home.calendar.newevent

data class NewEventItem(val iconId: Int, val description: String)