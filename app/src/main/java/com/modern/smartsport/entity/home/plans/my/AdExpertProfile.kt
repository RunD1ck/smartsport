package com.modern.smartsport.entity.home.plans.my
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AdExpertProfile(val nameAndSurname: String, val bio: String, val avatarResId: Int) : Parcelable