package com.modern.smartsport.entity.home.calendar.selectplace

data class FokDescriptionItem(val title: String, val description: String)