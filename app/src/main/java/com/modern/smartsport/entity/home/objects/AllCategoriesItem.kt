package com.modern.smartsport.entity.home.objects

data class AllCategoriesItem(
    val iconId: Int,
    val title: String,
    val description: String?,
    var isSelected: Boolean = false
)