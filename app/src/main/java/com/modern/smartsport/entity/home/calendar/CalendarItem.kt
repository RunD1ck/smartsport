package com.modern.smartsport.entity.home.calendar

data class CalendarDay(
    val weekDay: String,
    val monthDay: Int,
    val listEvents: ArrayList<BaseCalendar>,
    var isCurrentTime: Boolean = false
)

data class CalendarEvent(
    val id: Int,
    val title: String,
    val timeFrom: String,
    val timeTo: String,
    val description: String,
    var eventType: CalendarEventType = CalendarEventType.EXPIRE,
    var weekDay: String? = null,
    var monthDay: Int? = null,
    var isCurrentTime: Boolean = false
) : BaseCalendar()