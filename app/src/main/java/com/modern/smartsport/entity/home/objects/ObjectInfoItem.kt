package com.modern.smartsport.entity.home.objects

data class ObjectInfoItem(
    val sportKind: String,
    val nameObject: String,
    val streetObject: String,
    val rating: Float,
    val votesCount: Int,
    val timeOnFoot: String,
    val timeOnCar: String,
    val timeOnPublicTransport: String
)