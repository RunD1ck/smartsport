package com.modern.smartsport.entity.home.calendar

import com.modern.smartsport.entity.common.pagination.PaginationDTO

data class GetCalendarDTO(
    val pagination: PaginationDTO,
    val calendarDays: List<CalendarDay>
)

