package com.modern.smartsport.entity.home.plans.main

class Plan(val name: String, val subTitle: String, val message: String, val backgroundResId: Int)