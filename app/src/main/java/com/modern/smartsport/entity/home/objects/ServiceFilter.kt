package com.modern.smartsport.entity.home.objects

data class ServiceFilter(val title: String, var isSelected: Boolean = false) : Filter()