package com.modern.smartsport.entity.home.objects

data class ObjectTimeInfo(val iconId: Int, val time: String)