package com.modern.smartsport.presentation.viewmodel.home.profile.my.achievement

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.modern.smartsport.entity.home.profile.my.achievement.Achievement
import org.koin.core.KoinComponent
import org.koin.core.inject
import com.modern.smartsport.model.interactor.home.profile.my.achievement.AchievementInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception

class AchievementViewModel : BaseViewModel(), KoinComponent {

    private val interactor: AchievementInteractor by inject()

    val listAchievements: MutableLiveData<List<Achievement>> = MutableLiveData()

    init {
        getAchievements()
    }

    //Events

    //Private methods
    private fun getAchievements() {
        coroutineScope.launch {
            if (listAchievements.value == null) {
                try {
                    listAchievements.postValue(interactor.getAchievements())
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }
        }
    }
}
