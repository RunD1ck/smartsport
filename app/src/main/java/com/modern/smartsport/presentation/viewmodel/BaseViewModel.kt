package com.modern.smartsport.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.modern.smartsport.di.getNavScopeName
import com.modern.smartsport.di.globalNavScopeId
import com.modern.smartsport.ui.navigation.NavControllerNavigator
import com.modern.smartsport.ui.navigation.Navigator
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import org.koin.core.KoinComponent

open class BaseViewModel : ViewModel(), KoinComponent {

    private val exHandler = CoroutineExceptionHandler { _, _ -> }
    private val job = SupervisorJob()

    private val coroutineContext = Dispatchers.IO + exHandler + job
    protected val coroutineScope = CoroutineScope(coroutineContext)

    private fun getNavigator(navScopeId: String): Navigator =
        getKoin().getScope(navScopeId).get<NavControllerNavigator>()

    /**
     * Навигация вперед на уровне Activity
     * @param directions
     */
    fun globalTo(directions: NavDirections) = getNavigator(globalNavScopeId).to(directions)

    fun localTo(graphLabel: String, directions: NavDirections) =
        getNavigator(getNavScopeName(graphLabel)).to(directions)

    fun back(graphLabel: String){
        getNavigator(getNavScopeName(graphLabel)).back()
    }

    fun globalBack(){
        getNavigator(globalNavScopeId).back()
    }
}
