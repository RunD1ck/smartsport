package com.modern.smartsport.presentation.viewmodel.home.objects

import androidx.lifecycle.MutableLiveData
import com.modern.smartsport.entity.home.objects.ObjectInfoItem
import com.modern.smartsport.entity.home.objects.ServiceFilter
import com.modern.smartsport.model.interactor.home.objects.ObjectsInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber
import java.lang.Exception

class ObjectsViewModel : BaseViewModel(), KoinComponent {

    private val interactor: ObjectsInteractor by inject()
    val state = MutableLiveData<State>()
    val listServices: MutableLiveData<List<Any>> = MutableLiveData()
    val objectInfo: MutableLiveData<ObjectInfoItem> = MutableLiveData()


    init {
        state.postValue(State.ShowMap)
        initListServices()
    }

    sealed class State {
        object ShowMap : State()
        object ShowList : State()
    }

    //Events
    fun onToggleStateClick() {
        if (state.value == State.ShowList) state.postValue(State.ShowMap) else state.postValue(State.ShowList)
    }

    fun onMarkerClick(lat: Double, lon: Double) {
        coroutineScope.launch {
            try {
                objectInfo.postValue(interactor.getObjectInfo(lat, lon))
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    //Private methods
    private fun initListServices() {
        if (listServices.value == null) {
            coroutineScope.launch {
                try {
                    listServices.postValue(interactor.getListServices())
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }

        }
    }

}
