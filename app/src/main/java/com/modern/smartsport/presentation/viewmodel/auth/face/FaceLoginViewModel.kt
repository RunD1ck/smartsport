package com.modern.smartsport.presentation.viewmodel.auth.face

import com.modern.smartsport.model.interactor.auth.FaceLoginInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.auth.face.FaceLoginFragmentDirections
import com.modern.smartsport.utils.SingleLiveEvent
import org.koin.core.KoinComponent
import org.koin.core.inject

class FaceLoginViewModel : BaseViewModel(), KoinComponent {

    private val interactor: FaceLoginInteractor by inject()


    val state: SingleLiveEvent<State> = SingleLiveEvent()

    init {

    }

    sealed class State {
        object FinishFaceLogin : State()
    }

    //Events
    fun setOnScanBtnClick() {
        state.postValue(State.FinishFaceLogin)
    }

    fun setOnFinishBtnClick() {
        globalTo(FaceLoginFragmentDirections.actionFaceLoginFragmentToHomeFragment())
    }

    //Private methods
}
