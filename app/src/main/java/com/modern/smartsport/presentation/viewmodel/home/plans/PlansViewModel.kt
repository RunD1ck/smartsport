package com.modern.smartsport.presentation.viewmodel.home.plans

import com.modern.smartsport.model.interactor.home.plans.PlansInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.home.plans.PlansFragmentDirections
import org.koin.core.KoinComponent
import org.koin.core.inject

class PlansViewModel : BaseViewModel(), KoinComponent {

    private val interactor: PlansInteractor by inject()

    init {

    }

    //Events
    fun onTraningPlanClick() {
        localTo("plans", PlansFragmentDirections.actionPlansFragmentToTraningFragment())
    }

    fun onNutritionClick() {
        localTo("plans", PlansFragmentDirections.actionPlansFragmentToNutritionFragment())
    }

    //Private methods
}
