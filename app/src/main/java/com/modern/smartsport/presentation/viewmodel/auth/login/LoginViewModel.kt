package com.modern.smartsport.presentation.viewmodel.auth.login

import com.modern.smartsport.model.interactor.auth.LoginInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.auth.login.LoginFragmentDirections
import org.koin.core.KoinComponent
import org.koin.core.inject

class LoginViewModel : BaseViewModel(), KoinComponent {

    private val interactor: LoginInteractor by inject()

    init {

    }

    //Events
    fun onLoginPhoneBtnClick() {
        globalTo(LoginFragmentDirections.actionLoginFragmentToPhoneLoginFragment())
    }

    fun onLoginFaceBtnClick() {
        globalTo(LoginFragmentDirections.actionLoginFragmentToFaceLoginFragment())
    }

    fun onLoginWithoutRegistrationBtnClick() {
        globalTo(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
    }

    //Private methods
}
