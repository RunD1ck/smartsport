package com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.selectplace.selectdate

import org.koin.core.KoinComponent
import org.koin.core.inject
import com.modern.smartsport.model.interactor.home.calendar.newevent.selectplace.selectdate.SelectDateInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.home.calendar.newevent.selectplace.selectdate.SelectDateFragmentDirections

class SelectDateViewModel : BaseViewModel(), KoinComponent {

    private val interactor: SelectDateInteractor by inject()

    init {

    }

    //Events
    fun onCalendarViewClick(){
        localTo("calendar",SelectDateFragmentDirections.actionSelectDateFragmentToReservationFragment())
    }

    fun onBtnOkClick(){
        localTo("calendar",SelectDateFragmentDirections.actionSelectDateFragmentToSelectTimeFragment())
    }

    //Private methods
}
