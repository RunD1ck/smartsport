package com.modern.smartsport.presentation.viewmodel.home

import org.koin.core.KoinComponent
import org.koin.core.inject
import com.modern.smartsport.model.interactor.home.HomeInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel

class HomeViewModel : BaseViewModel(), KoinComponent {

    private val interactor: HomeInteractor by inject()

    init {

    }

    //Events

    //Private methods
}
