package com.modern.smartsport.presentation.viewmodel.home.calendar.newevent

import com.modern.smartsport.R
import com.modern.smartsport.entity.home.calendar.newevent.NewEventItem
import org.koin.core.KoinComponent
import org.koin.core.inject
import com.modern.smartsport.model.interactor.home.calendar.newevent.NewEventInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.home.calendar.newevent.NewEventFragmentDirections

class NewEventViewModel : BaseViewModel(), KoinComponent {

    private val interactor: NewEventInteractor by inject()

    init {

    }

    //Events
    fun onItemSelectPlaceClick() {
        localTo(
            "calendar",
            NewEventFragmentDirections.actionNewEventFragmentToSelectPlaceFragment()
        )
    }

    //Private methods
}
