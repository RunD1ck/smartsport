package com.modern.smartsport.presentation.viewmodel.home.profile.my.form

import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import org.koin.core.KoinComponent
import org.koin.core.inject
import com.modern.smartsport.model.interactor.home.profile.my.form.MyFormInteractor

class MyFormViewModel : BaseViewModel(), KoinComponent {

    private val interactor: MyFormInteractor by inject()

    init {

    }

    //Events

    //Private methods
}
