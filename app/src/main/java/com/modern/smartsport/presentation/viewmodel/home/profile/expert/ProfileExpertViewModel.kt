package com.modern.smartsport.presentation.viewmodel.home.profile.expert

import com.modern.smartsport.model.interactor.home.profile.expert.ProfileExpertInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.home.HomeFragmentDirections
import org.koin.core.KoinComponent
import org.koin.core.inject

class ProfileExpertViewModel : BaseViewModel(), KoinComponent {

    private val interactor: ProfileExpertInteractor by inject()

    init {

    }

    //Events
    fun allCommentsBtnClick() {
        globalTo(HomeFragmentDirections.actionHomeFragmentToAllCommentsFragment())
    }


    //Private methods
}
