package com.modern.smartsport.presentation.viewmodel.home.profile.my

import com.modern.smartsport.model.interactor.home.profile.my.ProfileInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.home.HomeFragmentDirections
import com.modern.smartsport.ui.fragment.home.profile.my.ProfileFragmentDirections
import org.koin.core.KoinComponent
import org.koin.core.inject

class ProfileViewModel : BaseViewModel(), KoinComponent {

    private val interactor: ProfileInteractor by inject()

    init {

    }

    //Events
    fun onExitItemClick() {
        globalTo(HomeFragmentDirections.actionHomeFragmentToLoginFragment())
    }

    fun onAchievementBtnClick() {
        localTo("profile", ProfileFragmentDirections.actionProfileFragmentToAchievementFragment())
    }

    //Private methods
}
