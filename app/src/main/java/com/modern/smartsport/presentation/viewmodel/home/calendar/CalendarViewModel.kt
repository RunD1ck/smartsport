package com.modern.smartsport.presentation.viewmodel.home.calendar

import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.modern.smartsport.entity.home.calendar.BaseCalendar
import com.modern.smartsport.model.interactor.home.calendar.CalendarInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.common.pagination.CalendarDataSource
import com.modern.smartsport.ui.fragment.home.calendar.CalendarFragmentDirections
import org.koin.core.KoinComponent
import org.koin.core.inject

class CalendarViewModel : BaseViewModel(), KoinComponent {

    private val interactor: CalendarInteractor by inject()
    val calendarEventsList: MutableLiveData<PagedList<BaseCalendar>> = MutableLiveData()

    init {
        getCalendar()
    }

    companion object {
        const val PAGE_SIZE = 4
    }

    //Events

    fun onBtnEventClick(){
        localTo("calendar",CalendarFragmentDirections.actionCalendarFragmentToNewEventFragment())
    }

    //Private methods
    private fun getCalendar() {
        val pagedListLiveData = LivePagedListBuilder(
            CalendarDataSource.Factory(
                coroutineScope,
                interactor,
                PAGE_SIZE.toString()
            ),
            PagedList
                .Config
                .Builder()
                .setEnablePlaceholders(false)
                .setPageSize(PAGE_SIZE)
                .build()
        )
            .build()
        pagedListLiveData.observeForever {
            calendarEventsList.value = it
        }
    }
}
