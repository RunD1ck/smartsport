package com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.selectplace

import android.util.Log
import androidx.lifecycle.MutableLiveData
import org.koin.core.KoinComponent
import org.koin.core.inject
import com.modern.smartsport.model.interactor.home.calendar.newevent.selectplace.SelectPlaceInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.home.calendar.newevent.selectplace.SelectPlaceFragmentDirections
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class SelectPlaceViewModel : BaseViewModel(), KoinComponent {

    private val interactor: SelectPlaceInteractor by inject()

    init {
    }

    //Events

    fun onBtnSelectDateClick() {
        localTo(
            "calendar",
            SelectPlaceFragmentDirections.actionSelectPlaceFragmentToSelectDateFragment()
        )
    }

    fun onBtnReservationClick() {
        localTo(
            "calendar",
            SelectPlaceFragmentDirections.actionSelectPlaceFragmentToChooseReservationFragment()
        )
    }

    //Private methods


}
