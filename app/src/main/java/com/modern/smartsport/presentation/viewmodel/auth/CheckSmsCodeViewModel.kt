package com.modern.smartsport.presentation.viewmodel.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.modern.smartsport.model.interactor.auth.CheckSmsCodeInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.auth.check_code.CheckSmsCodeFragmentDirections
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber

class CheckSmsCodeViewModel : BaseViewModel(), KoinComponent {

    private val interactor: CheckSmsCodeInteractor by inject()


    //Events
    fun onEtCodeEntered(smsCode: String) {
        coroutineScope.launch {
            try {
                if (interactor.sendCode(smsCode)) {
                    globalTo(CheckSmsCodeFragmentDirections.actionCheckSmsCodeFragmentToHomeFragment())
                }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun onTvCloseClick(){
        globalBack()
    }

    //Private methods

}
