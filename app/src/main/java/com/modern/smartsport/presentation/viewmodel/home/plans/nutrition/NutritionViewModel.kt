package com.modern.smartsport.presentation.viewmodel.home.plans.nutrition

import androidx.lifecycle.MutableLiveData
import com.modern.smartsport.model.interactor.home.plans.nutrition.NutritionInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import org.koin.core.KoinComponent
import org.koin.core.inject

class NutritionViewModel : BaseViewModel(), KoinComponent {

    private val interactor: NutritionInteractor by inject()
    val nutritionViewModelState: MutableLiveData<State> = MutableLiveData()

    init {
        nutritionViewModelState.postValue(State.EmptyTraningProgram)
    }

    //State
    sealed class State {
        object EmptyTraningProgram : State()
        object NoEmptyTraningProgram : State()
    }

    //Events

    //Private methods
}
