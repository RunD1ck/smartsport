package com.modern.smartsport.presentation.viewmodel.auth

import android.util.Log
import com.modern.smartsport.model.interactor.auth.PhoneLoginInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.auth.phone.PhoneLoginFragmentDirections
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber


class PhoneLoginViewModel : BaseViewModel(), KoinComponent {

    private val interactor: PhoneLoginInteractor by inject()

    init {

    }

    //Events
    fun onBtnSendClick(phoneNumber: String) {
        coroutineScope.launch {
            try {
                interactor.sendPhone(phoneNumber)
                globalTo(PhoneLoginFragmentDirections.actionPhoneLoginFragmentToCheckSmsCodeFragment())

            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun onTvCloseClick(){
        globalBack()
    }

    //Private methods
}
