package com.modern.smartsport.presentation.viewmodel.home.profile.expert.scholars

import org.koin.core.KoinComponent
import org.koin.core.inject
import com.modern.smartsport.model.interactor.home.profile.expert.scholars.AllScholarsInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel

class AllScholarsViewModel : BaseViewModel(), KoinComponent {

    private val interactor: AllScholarsInteractor by inject()

    init {

    }

    //Events

    //Private methods
}
