package com.modern.smartsport.presentation.viewmodel.home.profile.comments

import com.modern.smartsport.model.interactor.home.profile.comments.AllCommentsInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import org.koin.core.KoinComponent
import org.koin.core.inject

class AllCommentsViewModel : BaseViewModel(), KoinComponent {

    private val interactor: AllCommentsInteractor by inject()

    init {

    }

    //Events

    //Private methods
}
