package com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.selectplace.successreservation

import org.koin.core.KoinComponent
import org.koin.core.inject
import com.modern.smartsport.model.interactor.home.calendar.newevent.selectplace.succesresrvation.SuccessReservationInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel

class SuccessReservationViewModel : BaseViewModel(), KoinComponent {

    private val interactor: SuccessReservationInteractor by inject()

    init {

    }

    //Events

    //Private methods
}
