package com.modern.smartsport.presentation.viewmodel.home.calendar.newevent.selectplace.choosedate

import org.koin.core.KoinComponent
import org.koin.core.inject
import com.modern.smartsport.model.interactor.home.calendar.newevent.selectplace.choosedate.ChooseDateInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel

class ChooseDateViewModel : BaseViewModel(), KoinComponent {

    private val interactor: ChooseDateInteractor by inject()

    init {

    }

    //Events

    //Private methods
}
