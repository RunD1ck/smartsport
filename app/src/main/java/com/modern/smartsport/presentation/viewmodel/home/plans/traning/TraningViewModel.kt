package com.modern.smartsport.presentation.viewmodel.home.plans.traning

import androidx.lifecycle.MutableLiveData
import com.modern.smartsport.entity.home.plans.my.AdExpertProfile
import com.modern.smartsport.model.interactor.home.plans.traning.TraningInteractor
import com.modern.smartsport.presentation.viewmodel.BaseViewModel
import com.modern.smartsport.ui.fragment.home.plans.traning.TraningFragmentDirections
import org.koin.core.KoinComponent
import org.koin.core.inject

class TraningViewModel : BaseViewModel(), KoinComponent {

    private val interactor: TraningInteractor by inject()
    val adTrainsExpert: MutableLiveData<List<AdExpertProfile>> = MutableLiveData()
    val traningViewModelState: MutableLiveData<State> = MutableLiveData()

    init {
        adTrainsExpert.postValue(interactor.getAdExpert())
        traningViewModelState.postValue(State.EmptyTraningProgram)
    }

    //State
    sealed class State {
        object EmptyTraningProgram : State()
        object NoEmptyTraningProgram : State()
    }

    //Events
    fun signUpClick(adExpertProfile: AdExpertProfile) {
        localTo(
            "plans",
            TraningFragmentDirections.actionTraningFragmentToProfileExpertFragment(adExpertProfile)
        )
    }

    fun onBtnGetTraningProgramClick() {
        traningViewModelState.postValue(State.NoEmptyTraningProgram)
    }

    //Private methods
}
