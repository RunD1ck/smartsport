plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(29)
    defaultConfig {
        applicationId = "com.modern.smartsport"
        minSdkVersion(21)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        val options = this as org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions
        options.jvmTarget = "1.8"
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    packagingOptions {
        exclude("META-INF/*.kotlin_module")
    }

    androidExtensions {
        isExperimental = true
    }
}

androidExtensions {
    isExperimental = true
}

dependencies {
    val kotlin_version = rootProject.extra["kotlin_version"]
    val ktor_version = rootProject.extra["ktor_version"]
    val koin_version = rootProject.extra["koin_version"]
    val navigation_version = rootProject.extra["navigation_version"]
    val delegate_adapter = rootProject.extra["delegate_adapter"]

    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version")
    implementation("androidx.appcompat:appcompat:1.1.0")
    implementation("androidx.core:core-ktx:1.1.0")

    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation("com.google.android.material:material:1.0.0")
    testImplementation("junit:junit:4.12")
    androidTestImplementation("androidx.test.ext:junit:1.1.1")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.2.0")

    //arch components
    // ViewModel and LiveData
    implementation("androidx.lifecycle:lifecycle-extensions:2.1.0")

    //Coroutines
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.3")

    //Ktor
    implementation("io.ktor:ktor-client-okhttp:$ktor_version")
    implementation("io.ktor:ktor-client-json-jvm:$ktor_version")

    //Kotlin mpp
    implementation(project(":mpp"))

    //Maps
    implementation("com.google.android.gms:play-services-maps:17.0.0")

    // Koin for Kotlin
    implementation("org.koin:koin-core:$koin_version")
    implementation("org.koin:koin-android-scope:$koin_version")

    // Navigation
    implementation("androidx.navigation:navigation-runtime-ktx:$navigation_version")
    implementation("androidx.navigation:navigation-fragment-ktx:$navigation_version")
    implementation("androidx.navigation:navigation-ui-ktx:$navigation_version")

    //Glide
    implementation("com.github.bumptech.glide:glide:4.11.0")
    annotationProcessor("com.github.bumptech.glide:compiler:4.11.0")
    implementation("jp.wasabeef:glide-transformations:4.0.0")

    //UI
    implementation("com.redmadrobot:input-mask-android:5.0.0")
    implementation("com.hannesdorfmann:adapterdelegates4:$delegate_adapter")
    implementation("com.hannesdorfmann:adapterdelegates4-pagination:$delegate_adapter")
    implementation("com.hannesdorfmann:adapterdelegates4-kotlin-dsl:$delegate_adapter")
    implementation("com.jakewharton.timber:timber:4.7.1")

    //VereficationEditText
    implementation("com.alimuzaffar.lib:pinentryedittext:2.0.6")
}